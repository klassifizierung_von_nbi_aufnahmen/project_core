## Laryngoscopic Research Project

Here's a breakdown for how this project is organized.

The `libs` folder contains code used throughout the project and experiments. This includes
code for rendering images, extracting data, and calculating statistics.

The `experiments` folder contains the jupyter notebooks which were built and run for the research.

The `datasets` folder contains code and notebooks for building up the final dataset. At `datasets/select_roi/app.py` there is also a tiny python program which allows for interactively selecting ROIs in images. It does contain these original and partly augmented datasets as well for convenience.

The `eda` folder contains initial data exploration, some baseline experiments and older notebooks and test runs for
trying out transfer learning and other ideas. The paths will need to be updated on these scripts when importing libraries and files because the scripts themselves have been moved around a bit.


#### Installation

It is assumed you are using anaconda and Python 3.6+. If you aren't you can install it
for your system at [anaconda](https://www.anaconda.com/download/).

Installing required libraries.

```
pip install -r requirements.txt
```

Ensure you have tensorflow and keras installed,

For anaconda this is:

```
conda install -c conda-forge tensorflow
conda install -c conda-forge keras
```

#### The Data

The data is stored in one huge zip file. Inside the zip file folders are organized based on the experiment numbers. These have the same names as the project. Simply copy the folders to the corresponding `experiement_data/<experiment>/` path.

Here's an example:

```
# example of extracting the experiment data to the target experiment folder.
unzip experiment_data.zip
cp -R experiment_data/03_papilloma_v_dysplasia+carcinoma/315f7638/ experiments/03_papilloma_v_dysplasia+carcinoma/
```

As for the laryngeal dataset used in the verification experiment, it can be found here: https://zenodo.org/record/1003200#.Wus8YXWFNhE

Simply download and extract it into the `experiments/07_validation/` directory root. You might have to
rename it for the jupyter notebook to work. That should be it though.


#### Experiments

Experiment directories are organized as follows:

```
experiment_name/
  runs/
    001_run_name.ipynb
    002_another_name.ipynb
    ...
    00N_file.ipynb
  01_Intro.ipynb
  02_Data_Prep.ipynb
  02_Data_Prep_SP.ipynb
  notes.md
```

The notes and Intro files can be ignored since those were just my random thoughts at different stages of the experiment and they can be confusing and misleading since I copied and pasted stuff around in them.
