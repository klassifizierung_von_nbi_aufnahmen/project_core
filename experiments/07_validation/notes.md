## Validation Experiment

### 001 Simple v1

This experiment revelead that with some very aggressive data augmentation our CNN can perform roughly than 95% on specificity.

#### Key Takeaways
1. In order to get over 90% performance we need to do aggressive data augmentation b/c the dataset is so small.
2. Data Augmentation was able to remove any confusion between Healthy Tissue and Leukoplakia.

#### Results Summary
1. Our CNN had no problems differentiating healthy tissue and leukoplakia.
2. Misclassifications were of IPCL and Hbv.