from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Conv2D, MaxPool2D, Activation
from keras.layers.normalization import BatchNormalization
from keras.optimizers import RMSprop
from keras.callbacks import ReduceLROnPlateau

def optimizer(learning_rate = 0.001):
    return RMSprop(lr=learning_rate, rho=0.9, epsilon=1e-08, decay=0.0)

def annealer():
    # Set a learning rate annealer
    return ReduceLROnPlateau(monitor='val_acc', patience=3, verbose=1, factor=0.5, min_lr=0.00001)

def run_deep_dropout():
    """
    This model is still a very simple one, it only has 3 hidden layers and uses the sigmoid activation function.
    
    The big difference between it and `shallow_model_v1` is that it add a Regularization method : Dropout
    """
    
    model = Sequential()

    model.add(Conv2D(filters = 32, kernel_size = (3,3),padding = 'Same', input_shape = (101,101,3)))
    model.add(Activation('relu'))
    model.add(Conv2D(filters = 32, kernel_size = (3,3), padding = 'Same'))
    model.add(Activation('relu'))
    model.add(MaxPool2D(pool_size=(2,2)))
    model.add(Dropout(0.25))

    model.add(Conv2D(filters = 64, kernel_size = (3,3), padding = 'Same'))
    model.add(Activation('relu'))
    model.add(Conv2D(filters = 64, kernel_size = (3,3),padding = 'Same'))
    model.add(Activation('relu'))
    model.add(MaxPool2D(pool_size=(2,2), strides=(2,2)))
    model.add(Dropout(0.25))

    model.add(Conv2D(filters = 48, kernel_size = (3,3), padding = 'Same'))
    model.add(Activation('relu'))
    model.add(Conv2D(filters = 32, kernel_size = (3,3),padding = 'Same'))
    model.add(Activation('relu'))
    model.add(MaxPool2D(pool_size=(2,2), strides=(2,2)))
    model.add(Dropout(0.25))
    
    model.add(Flatten())
    model.add(Dense(1024))
    model.add(Activation('relu'))
    model.add(Dropout(0.5))
    model.add(Dense(4, kernel_initializer='normal', activation = "sigmoid"))

    return model