## Experiments

### Run 1 : Simple 3 Layer
Precision (specificity): 54.105%
Recall (sensitivity): 50.343%
Accuracy: 72.385%
F1: 48.794%


### Run 2 : Simple 3 Layer (no Data Aug)
Precision (specificity): 56.849%
Recall (sensitivity): 59.646%
Accuracy: 67.364%
F1: 55.402%

### Run 3 : Simple 3 Layer (Data Aug)
Precision (specificity): 64.719%
Recall (sensitivity): 61.408%
Accuracy: 75.314%
F1: 58.609%

### Run 4 : Research Model (no Aug)
Precision (specificity): 46.361%
Recall (sensitivity): 47.311%
Accuracy: 68.619%
F1: 45.646%

### Run 5: Research Model (Data Aug)
