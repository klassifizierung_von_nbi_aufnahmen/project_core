## Experiments

### Run 1 : No Regularization
Precision (specificity): 72.713%
Recall (sensitivity): 72.134%
Accuracy: 71.944%
F1: 71.808%

#### Summary
1. Misclassified benign tissues that visually have the same green qualities of carcinomas.
2. Never misclassified benign, but confused a lot of carcinoma+dysplasia tissues for benign.
3. Misclassfied contour edges, but these regions largely appear to be benign in nature, so actually it is classifying correctly.


### Run 2 : Dropout
batch size = 16, epochs = 20

Precision (specificity): 73.111%
Recall (sensitivity): 71.677%
Accuracy: 71.389%
F1: 71.013%

#### Summary
1. Adding dropout allowed the model to much better differentiate between benign and carcinoma tissue.
2. However, it did think a decent proportion of benign tissues were carcinom+dysplasia in nature.
3. The models are getting confused when it is benign tissue but there is a film (Leukoplakia) or general smoooth green texture to an area.


### Run 3 : Deeper Network with Data Augmentation
batch size = 64, epochs = 4 with early stopping

Precision (specificity): 71.182%
Recall (sensitivity): 69.491%
Accuracy: 69.167%
F1: 68.632%

#### Summary
1. Adding Data Augmentation increased the ability of the model to correctly identify carcinoma+dysplasia tissues.
2. It still misclassified tissues that were carcinomas but looked benign to the naked eye.
3. It misclassified contour edges that looked benign as well.


### Run 4 : Research Model with Data Augmentation and No Regularization
batch size = 64, epochs = 6, no early stopping

Precision (specificity): 74.086%
Recall (sensitivity): 73.024%
Accuracy: 72.778%
F1: 72.533%

#### Summary
1. Same problems as above exeriments. Contour edge confusions, mislabelling carcinoma+dysplasia tissues that visually are similar to benign ones. 

### Run 5 : Research Model with Data Augmentation and Regularization + SP Dataset
batch size = 64, epochs = 6, no early stopping

Precision (specificity): 91.541%
Recall (sensitivity): 88.206%
Accuracy: 89.568%
F1: 89.041%

#### Summary
1. We still misclassified a green benign structure.
2. We misclassifed contour edges, but this looks correct. The edges were likely carcinoma in nature.
3. Able to correctly predict 155/157 carcinoma+dysplasia patches, misclassified some benigns but that is ok.