## Experiments

### Run 1
Precision (specificity): 72.845%
Recall (sensitivity): 79.134%
Accuracy: 82.338%
F1: 74.958%

#### Notes
1. Thinks a lot of carcinomas+dysplasias are actually papillomas
2. Contours 5/6 (barely got one ROI 0.51 over 0.49)

### Run 2 : Only 3 Iterations w / Data Aug
Precision (specificity): 70.552%
Recall (sensitivity): 75.549%
Accuracy: 80.779%
F1: 72.301%

#### Notes
1. Probably not enough epochs (only 3)


### Run 3 : Research Model (no Regularization)
Precision (specificity): 76.714%
Recall (sensitivity): 77.544%
Accuracy: 85.714%
F1: 77.115%

#### Notes
1.