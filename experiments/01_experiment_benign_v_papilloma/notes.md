## Experiments

### 001 Simple v1

This experiment revealed a lot. Initially, I had problems getting the model to train on even the simplest data.
I tried to overfit and just learn the values of 10 images and it still wasn't able to do that. This eventually led me to explore
two facets that are key to building ConvNets with small datasets: batch size and label distribution per batch.

It turns out that many of my initial batch sizes such as 32 and 64 contained only 1 type of label and so the model couldn't learn anything new
in each batch. The fix for this was to randomize the input data a lot to make sure the two classes were mixed up very nicely with one another.
Then during each batch the model had different class labels to look at and learn from and couldn't move itself into a optimization valley.

Finally, I began with a complex ConvNet Model and wasn't able to figure out the bugs because there were too many variables.
From now on I will always start with a very simple model that does not generalize at all (no dropout, no regularization), and 
move on from there.

#### Key Takeaways
1. Watch batch_size on small datasets
2. Really inspect how well the labels are distributed within each batch.
3. Make sure you aren't training your model with batches that contain all the same labels.
4. Always start with a very simple model and get training accuracy to 100%, move on from there.


#### Results Summary
1. It has difficulty on contour boundaries between benign and papilloma tissue, but its actually correctly classifying in most cases.
2. Seems to get confused around smooth green tissues. Often papillomas at a distance are difficult to distinguish between benign green tissues that are close up.
3. Little problems recognizing red benign tissues.
4. Can recognize green tissues that are bulbous and have clear papilloma geometry as papillomas.