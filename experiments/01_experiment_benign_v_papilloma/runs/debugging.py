import sys
sys.path.append('../')
sys.path.append('../../../')
# keras expects data to be encoded in a particular way, to_categorical does this encoding for us (one-hot encoding)
from keras.utils.np_utils import to_categorical
import libs.importing as importing
import pandas as pd
import os
import models
import libs.normalization as normalization
from importlib import reload
import libs.data_prep as data_prep
import keras.optimizers

# LOAD PATCH DATA REFERENCES

# directory setup
data_source = '../6ceac90d'
train_dir = os.path.join(data_source, 'train')
test_dir = os.path.join(data_source, 'test')
val_dir = os.path.join(data_source, 'val')

# get references to the data we want
train_patches_df = pd.read_csv(os.path.join(train_dir, 'patches_cleaned.csv'))
train_data_reader = importing.PatchDataReader(os.path.join(train_dir, 'patches'), train_patches_df)

test_patches_df = pd.read_csv(os.path.join(test_dir, 'patches_cleaned.csv'))
test_data_reader = importing.PatchDataReader(os.path.join(test_dir, 'patches'), test_patches_df)

val_patches_df = pd.read_csv(os.path.join(val_dir, 'patches_cleaned.csv'))
val_data_reader = importing.PatchDataReader(os.path.join(val_dir, 'patches'), val_patches_df)


## READ PATCH DATA

# load train, test, and validation data
x_train, y_train = train_data_reader.read_data()
x_test, y_test = test_data_reader.read_data()
x_val, y_val = val_data_reader.read_data()

# LOG RESULTS
## Visualize the patches
data_prep.log_patch_data_reader(train_dir, train_data_reader)
data_prep.log_patch_data_reader(test_dir, test_data_reader)
data_prep.log_patch_data_reader(val_dir, val_data_reader)

# shuffle the data randomly
x_train2, y_train2 = data_prep.shuffle_two_arrays(x_train, y_train)
x_test2, y_test2 = data_prep.shuffle_two_arrays(x_test, y_test)

## CONVERT CLASSES TO KERAS FORMAT
# keras expects classes to be number from 0-N, we are using 1-N numbering so we need to convert.
# then we also have to change the classes to one-hot encoding

y_train_oh = to_categorical(y_train2 - 1)
y_test_oh  = to_categorical(y_test2 - 1)
y_val_oh   = to_categorical(y_val - 1)

# NORMALIZATION

m, std = normalization.dataset_mean_and_std(x_train)

x_train = (x_train - m) / std
x_test  = (x_test - m) / std
x_val   = (x_val - m) / std

## MODEL : load from file, show summary, and compile
reload(models)

model = models.shallow_model_v1()

# parameters:
learning_rate = 1e-3

#optimizer = keras.optimizers.SGD(lr=learning_rate, momentum=0.0, decay=0.0, nesterov=False)

model.compile(optimizer = 'adam', loss = "binary_crossentropy", metrics=["accuracy"])


## RUN!!!

# parameters
batch_size = 32
epochs = 5


from tensorflow.python import debug as tf_debug
import keras.backend as K

#sess = K.get_session()
#sess = tf_debug.LocalCLIDebugWrapperSession(sess)
#K.set_session(sess)

history = model.fit(x_train[:100],y_train_oh[:100], batch_size=batch_size,
                              epochs = epochs,
                              validation_data = (x_test[:20],y_test_oh[:20]), shuffle=True,
                              verbose = 1, callbacks=[models.annealer()])
