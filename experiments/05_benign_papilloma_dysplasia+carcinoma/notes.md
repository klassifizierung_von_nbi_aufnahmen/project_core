## Experiments

### Run 1 : Very Simple Model with no Data Augmentation, no Regularization
Precision (specificity): 78.048%
Recall (sensitivity): 74.625%
Accuracy: 77.491%
F1: 72.833%


#### Summary
1. Dataset (Prime)
2. As predicted we are able to differentiate between the various tissues pretty well.


### Run 2 : Very Simple Model with Regularization (No Data Augmentation)
Precision (specificity): 72.468%
Recall (sensitivity): 70.309%
Accuracy: 72.325%
F1: 68.814%


#### Summary
1. Dataset (Prime)
2. Performance dropped when added regularization, we probably just overfit the model too much letting it run so many epochs.



### Run 3 : Simple Model with Regularization & Data Augmentation
Precision (specificity): 81.974%
Recall (sensitivity): 66.269%
Accuracy: 71.587%
F1: 60.975%

#### Summary
1. Wasn't able to distinguish b/t benign / papilloma and papilloma / dysplasia.



### Run 4 : Simple Model with Regularization & Data Augmentation
Precision (specificity): 81.974%
Recall (sensitivity): 66.269%
Accuracy: 71.587%
F1: 60.975%

#### Summary
1. I used this run to analyze different optimizers.
2. Adam optimizer performs much better than RMSprop which is what we had been using.


### Run 4 : Simple Model with Regularization & Data Augmentation (SP) Dataset
Precision (specificity): 81.370%
Recall (sensitivity): 64.508%
Accuracy: 66.355%
F1: 65.348%

#### Summary
1. Little difference in performance of model.
2. Did a good job of differentiating between benign and papilloma.
3. Confused a fair amount of benigns and carcinomas.
4. Confused some papillomas and carcinomas.
5. Edge confusion as usual.


### Run 5 : Research Model w/Reg no DataAug (SP)
Precision (specificity): 81.878%
Recall (sensitivity): 57.519%
Accuracy: 60.748%
F1: 55.967%

