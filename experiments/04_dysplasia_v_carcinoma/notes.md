## Experiments

### Run 1 : Research Model w/Dropout and no Data Augmentation
Precision (specificity): 21.728%
Recall (sensitivity): 50.000%
Accuracy: 43.455%
F1: 30.292%

-- all statistics not really applicable, b/c model just learned to guess the majority class every time.

#### Summary
1. Dataset (Subprime)
2. Dataset was too biases


### Run 2 : Research Model no Dropout and Data Augmentation
Precision (specificity): 21.728%
Recall (sensitivity): 50.000%
Accuracy: 43.455%
F1: 30.292%

-- all statistics not really applicable, b/c model just learned to guess the majority class every time.

#### Summary
1. Dataset (Subprime)
2. Too biased could not learn.


### Run 3 : Very Simple Model w/Dropout
Precision (specificity): 46.469%
Recall (sensitivity): 49.024%
Accuracy: 43.455%
F1: 34.968%

#### Summary
1. Dataset (Subprime)
2. We were actually able to learn with this much simpler model.
3. Our model cannot differentiate between dysplasias and carcinomas in our dataset.