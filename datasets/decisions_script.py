import numpy as np
from glob import glob
import pandas as pd
import os
import re

image_paths = glob('./decisions/**/*.jpg')
cwd = os.getcwd()

df = pd.DataFrame(columns=['image', 'classification', 'path_to_image'])

def get_class(img_path):
    if '01_good' in img_path:
        return 'good'
    elif '02_maybe' in img_path:
        return 'maybe'
    elif '03_bad' in img_path:
        return 'bad'
    elif '04_zoomed' in img_path:
        return 'zoomed'
    else:
        raise Exception('unknown classification.')

idx = 0
for img_path in image_paths:
    img_name = os.path.basename(img_path).replace('.jpg', '')
    short_path = img_path.replace(cwd, '')
    klass = get_class(img_path)
    df.loc[idx] = [img_name, klass, short_path]
    idx += 1

df.to_csv('./decisions/info.csv', index=False)
