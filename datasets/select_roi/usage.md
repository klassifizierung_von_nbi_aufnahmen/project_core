## Usage

This program loads and image or a matlab file and lets you draw contours on it.
It then saves these contours to an external directory of your choosing.

To try out the example run the following:

```bash
mkdir tmp
python app.py -i hgv-1a96dad1.jpg -o tmp/
```

## How the Program Works :

The program lets you use the mouse to click around and create contours.
All commands are done via pressing keyboard shortcut buttons.

They are:

1. S = Save Contour to directory
2. Q = Quit the program
3. C = Close a polygonal region
4. R = Print the contour vector points to the console

After saving the contour is cleared (but the screen is not updated) and you can select your next contour.
There is currently no way to "clear" a selection and reset. If you need to reset just restart the program.
