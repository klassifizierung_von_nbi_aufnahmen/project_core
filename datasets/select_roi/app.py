# import the necessary packages
import argparse
import cv2
import os
import json
import numpy as np
import pandas as pd
import scipy.io as sio

# initialize the list of reference points and boolean indicating
# whether cropping is being performed or not
refPt = []
selection_closed = False
saves = 0

LINE_COLOR = (255,0,0)
RESOURCE_NAME = ''
SAVE_DIR = None


def click_and_crop(event, x, y, flags, param):
    # grab references to the global variables
    global refPt, selection_closed, image, clone

    # If the right button is clicked clear the reference vector
    if event == cv2.EVENT_RBUTTONUP:
        refPt = []

    # check to see if the left mouse button was released
    elif event == cv2.EVENT_LBUTTONUP:
        if selection_closed:
            refPt = []
            image = clone.copy()
            cv2.imshow("image", image)
            selection_closed = False
        # record the ending (x, y) coordinates and indicate that
        # the cropping operation is finished
        refPt.append((x, y))

        ref_len = len(refPt)

        # draw a line from the previous refPt to the current one
        if ref_len > 0:
            cv2.line(image, refPt[ref_len-2], refPt[ref_len-1], LINE_COLOR, 2)
        cv2.imshow("image", image)


def close_selection():
    global refPt, selection_closed

    p1 = refPt[0]
    p2 = refPt[len(refPt)-1]

    cv2.line(image, p1, p2, LINE_COLOR, 2)
    cv2.imshow("image", image)
    selection_closed = True

def parse_args():
    # construct the argument parser and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--image", required=False, help='Path to the image.')
    ap.add_argument("-m", "--mat", required=False, help='Path to the matlab .mat file to load.')
    ap.add_argument("-o", '--outdir', required=True, help='the output directory to store the ROIs in.')
    args = vars(ap.parse_args())
    return args

def save_selection_to_file():
    global saves, refPt, SAVE_DIR

    dest = '{}_{}.csv'.format(RESOURCE_NAME, saves)
    full_path = os.path.join(SAVE_DIR, dest)
    df = pd.DataFrame(refPt)
    df.to_csv(full_path, index=False, header=None)
    refPt = []
    saves += 1

def run_app_loop():
    global refPt, image
    while True:
        # display the image and wait for a keypress
        cv2.imshow("image", image)
        key = cv2.waitKey(1) & 0xFF

        # if the 'c' key is pressed, close the selection
        if key == ord('c'):
            close_selection()
        elif key == ord('s'):
            print('saving selection to file\nselection data: {}\n'.format(refPt))
            save_selection_to_file()
            print('{} total saves.'.format(saves))
        # if the 'r' key is pressed, print the refPt
        elif key == ord("r"):
            print(refPt)
        # if the 'q' key is pressed, break from the loop
        elif key == ord("q"):
            break
    # close all open windows
    cv2.destroyAllWindows()

def read_image(img_name):
    global RESOURCE_NAME, image, clone
    RESOURCE_NAME = os.path.basename(img_name).replace('.jpg', '')
    image = cv2.imread(img_name)
    clone = image.copy()

    return True

def read_matlab_file(path_to_file):
    global image, clone, RESOURCE_NAME

    RESOURCE_NAME = os.path.basename(path_to_file).replace('.mat', '')
    mat = sio.loadmat(path_to_file, matlab_compatible=True)
    new_image = mat['data']['bild'][0][0].astype(np.uint8)
    image = cv2.cvtColor(new_image, cv2.COLOR_BGR2RGB)

    clone = image.copy()

    return True

def main_app():
    global image, clone, RESOURCE_NAME, SAVE_DIR

    args = parse_args()

    args_passed = False

    if args['image']:
        args_passed = read_image(args['image'])
    elif args['mat']:
        args_passed = read_matlab_file(args['mat'])

    if not args_passed:
        print('unable to load image or matlab file, try again.')
        exit(1)

    if not os.path.exists(args['outdir']):
        print('directory: {} does not exist.'.format(args['outdir']))
    else:
        SAVE_DIR = args['outdir']

    # load the image, clone it, and setup the mouse callback function
    cv2.namedWindow('image')
    cv2.setMouseCallback('image', click_and_crop)

    run_app_loop()


if __name__ == '__main__':
    main_app()
