import sys
import logging

def create_stream_logger():
    root = logging.getLogger()
    root.setLevel(logging.DEBUG)

    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s [%(levelname)s]: %(message)s')
    ch.setFormatter(formatter)
    root.addHandler(ch)

    return root

# create the default logger
DEFAULT_LOGGER = create_stream_logger()
