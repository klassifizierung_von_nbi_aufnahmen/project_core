# ======================
# filename: analysis.py
# ======================
#
# This file provides functions for computing statistics and inspecting various
# phases of the research process, having functions for:
#
# 1. ROI statistics: recall, precision, accuracy
#   for more of an explanation of how these were computed see:
#   - https://www.quora.com/How-do-I-compute-precision-and-recall-for-a-multi-class-classification-problem-Can-I-only-compute-accuracy
#   - http://text-analytics101.rxnlp.com/2014/10/computing-precision-and-recall-for.html
#
# 2. Patch level ROI statistics - using sklearn.metrics
#
# 3. ROI debugging: i.e. printing model predictions compared to ground truth values.

import sys
sys.path.append('../')
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix, recall_score, precision_score, f1_score
import itertools
import numpy as np
import os
import libs.utils as utils
import libs.bounding_box as lbb
import cv2
import pandas as pd
import libs.render as render

def contour_statistics(reader, model, base_dir, norm_data):
    """
    Calculate model predictions and ground truths over all ROIs.

    Print the predictions and ground truths for debugging purposes

    reader is an instance of PatchDataReader
    model is a trained model
    base_dir is the source dir (either train, test, or val)
    """
    df = reader.df
    patch_size = reader.patch_size

    images_dir = os.path.join(base_dir, 'images')
    patches_dir = os.path.join(base_dir, 'patches')

    rois = np.unique(df['contour_id'].values)

    preds = []
    truths = []

    for roi in rois:
        tmp_df = df[df['contour_id'] == roi].reset_index(drop=True)
        image_id = tmp_df['image_id'].values[0]


        size = len(tmp_df)
        patch_images = np.zeros((size, patch_size, patch_size, 3), dtype=np.float32)
        y_true = tmp_df['label_id'].values[0] - 1 # convert to zero based

        for idx, row in tmp_df.iterrows():
            patch_images[idx]
            path_to_img = os.path.join(patches_dir, row['patch_name'])
            patch_img = cv2.imread(path_to_img)
            patch_images[idx] = patch_img

        patch_images_norm = (patch_images - norm_data[0]) / norm_data[1]

        y_pred_oh = model.predict(patch_images_norm)
        y_pred = np.argmax(y_pred_oh, axis = 1)

        # sum the columns
        y_pred_sum = np.sum(y_pred_oh, axis=0)
        y_pred_total = y_pred_sum / len(y_pred_oh)
        print(image_id, y_pred_total, y_true)
        preds.append(y_pred_total)
        truths.append(y_true)
    return (preds, truths)


def roi_cm(y_true, y_pred, num_classes=2):
    """
    This function takes in the ground truths y_true and the predictions y_pred
    and calculates a confusion matrix for the ROI contour predictions.
    """
    n = len(y_true)
    cm = np.zeros((num_classes, num_classes), dtype=np.float32)

    for idx in range(n):
        true_idx = y_true[idx]
        preds = y_pred[idx]
        #print(preds, true_idx)

        for j in range(num_classes):
            cm[true_idx, j] += preds[j]
            #print(cm)
        #print('---next---')

    #print(cm)
    return cm

def normalize_roi_cm(cm, p=1e4):
    """
    This function can be used to scale up the probabilities of ROI confusion matrices and convert them
    to integer values without losing much information.
    """
    return np.round(cm * p).astype(np.int)

def roi_stats_2class(cm):
    """
    Calculate precision, recall, and accuracy for binary classification ROI problems.
    """
    tp = cm[1][1]
    tn = cm[0][0]
    fp = cm[0][1]
    fn = cm[1][0]

    precision = tp / (tp + fp)
    recall = tp / (tp + fn)
    accuracy = (tp + tn) / np.sum(cm)

    print('Precision (specificity): {0:.3f}%'.format(precision * 100))
    print('Recall (sensitivity): {0:.3f}%'.format(recall * 100))
    print('Accuracy: {0:.3f}%'.format(accuracy * 100))

def roi_stats_3class(cm):
    """
    Calculate precision, recall, and accuracy for three-class ROI problems.
    Weight all classes the same.
    """
    tp = np.sum(cm.diagonal())

    r1 = cm[0][0] / np.sum(cm[:,0])
    p1 = cm[0][0] / np.sum(cm[0,:])
    r2 = cm[1][1] / np.sum(cm[:,1])
    p2 = cm[1][1] / np.sum(cm[1,:])
    r3 = cm[2][2] / np.sum(cm[:,2])
    p3 = cm[2][2] / np.sum(cm[2,:])

    print(r1,r2,r3,p1,p2,p3)

    accuracy = tp / np.sum(cm)

    recall = (r1 + r2 + r3) / 3
    precision = (p1 + p2 + p3) / 3

    print('Precision (specificity): {0:.3f}%'.format(precision * 100))
    print('Recall (sensitivity): {0:.3f}%'.format(recall * 100))
    print('Accuracy: {0:.3f}%'.format(accuracy * 100))

def roi_stats_4class(cm):
    """
    Calculate precision, recall, and accuracy for four-class ROI problems.
    Weight all classes the same.
    """
    tp = np.sum(cm.diagonal())

    r1 = cm[0][0] / np.sum(cm[:,0])
    p1 = cm[0][0] / np.sum(cm[0,:])
    r2 = cm[1][1] / np.sum(cm[:,1])
    p2 = cm[1][1] / np.sum(cm[1,:])
    r3 = cm[2][2] / np.sum(cm[:,2])
    p3 = cm[2][2] / np.sum(cm[2,:])
    r4 = cm[3][3] / np.sum(cm[:,3])
    p4 = cm[3][3] / np.sum(cm[3,:])

    accuracy = tp / np.sum(cm)

    recall = (r1 + r2 + r3 + r4) / 4
    precision = (p1 + p2 + p3 + p4) / 4

    print('Precision (specificity): {0:.3f}%'.format(precision * 100))
    print('Recall (sensitivity): {0:.3f}%'.format(recall * 100))
    print('Accuracy: {0:.3f}%'.format(accuracy * 100))

def plot_contour_predictions(reader, model, base_dir, norm_data, patches_file='patches.csv'):
    """
    Renders model predictions onto an image and stores that image inside the log directory
    for the dataset.
    """
    df = reader.df
    rois = np.unique(reader.df.contour_id.values)

    patch_size = reader.patch_size

    out_dir = os.path.join(base_dir, 'log', 'plot_contour_predictions')
    images_dir = os.path.join(base_dir, 'images')
    contours_dir = os.path.join(base_dir, 'contours')
    patches_dir = os.path.join(base_dir, 'patches')

    utils.remove_and_create_path(out_dir)

    df = pd.read_csv(os.path.join(base_dir, patches_file), index_col=False)

    images = np.unique(reader.df[reader.df['contour_id'].isin(rois)]['image_id'].values)

    for image_id in images:
        image_name = '{}.jpg'.format(image_id)
        image_path = os.path.join(images_dir, image_name)

        img = cv2.imread(image_path)
        img2 = img.copy()
        tmp_df = df[df['image_id'] == image_id].reset_index(drop=True)

        # grab the contour
        roi = tmp_df['contour_id'].values[0]

        y_true = tmp_df['label_id'].values

        # name of output file
        outname = '{}_{}_ground_truth.jpg'.format(image_id, roi)
        outname_pred = '{}_{}_pred.jpg'.format(image_id, roi)

        # fetch the contour
        kdf = pd.read_csv(os.path.join(contours_dir, roi + '.csv'), header=None, index_col=False)
        kdf.columns = ['x', 'y']
        kv = utils.convert_roi_dataframe_to_vector(kdf)

        # do predictions
        size = len(tmp_df)
        patch_images = np.zeros((size, patch_size, patch_size, 3), dtype=np.float32)

        for idx, patch_row in tmp_df.iterrows():
            new_img = np.zeros(img.shape)
            patch_img = cv2.imread(os.path.join(patches_dir, patch_row['patch_name']))
            patch_label = patch_row['label_id']

            # append image
            copy_patch = patch_img.copy()
            patch_images[idx] = copy_patch

            (pw, ph, _) = patch_img.shape

            px = patch_row['x']
            py = patch_row['y']

            # write label on patch_img
            font = cv2.FONT_HERSHEY_SIMPLEX
            patch_img = cv2.putText(patch_img,str(patch_label),(15,15), font, 0.5, (0,255,0),1, cv2.LINE_AA)

            img[py:(py+pw), px:(px+ph)] = patch_img

        # Draw ROI on image
        img = render.draw_roi_on_image(img, kv, patch_size)
        cv2.imwrite(os.path.join(out_dir, outname), img)

        # What does model predict?
        patch_images_norm = (patch_images - norm_data[0]) / norm_data[1]
        y_pred_oh = model.predict(patch_images_norm)
        y_pred = np.argmax(y_pred_oh, axis = 1) + 1     # convert back 1-indexed

        for idx, patch_row in tmp_df.iterrows():
            patch_img = cv2.imread(os.path.join(patches_dir, patch_row['patch_name']))
            patch_label = y_pred[idx]

            (pw, ph, _) = patch_img.shape

            px = patch_row['x']
            py = patch_row['y']

            # write label on patch_img
            font = cv2.FONT_HERSHEY_SIMPLEX
            patch_img = cv2.putText( patch_img, str(patch_label),(15,15), font, 0.5, (0,255,0),1, cv2.LINE_AA)

            img2[py:(py+pw), px:(px+ph)] = patch_img

        cv2.imwrite(os.path.join(out_dir, outname_pred), img2)


def plot_loss_and_accuracy_curves(history, save=False):
    """
    Plot the loss and accuracy curves for a training and validation set of runs.

    Create two plots on top of each other, where the first inspects training and validation loss for each epoch.
    The second plot compares training and validation accuracy for each epoch.
    """
    fig, ax = plt.subplots(2,1)
    ax[0].plot(history.history['loss'], color='b', label="Training loss")
    ax[0].plot(history.history['val_loss'], color='r', label="validation loss",axes =ax[0])
    legend = ax[0].legend(loc='best', shadow=True)

    ax[1].plot(history.history['acc'], color='b', label="Training accuracy")
    ax[1].plot(history.history['val_acc'], color='r',label="Validation accuracy")
    legend = ax[1].legend(loc='best', shadow=True)

    if save:
        plt.savefig('loss_and_accuracy_curve.png')
    else:
        plt.show()

def plot_confusion_matrix_core(cm, classes,
                          normalize=False,
                          save=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j],
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')

    if save:
        plt.savefig('plot_confusion_matrix.png')
    else:
        plt.show()


def plot_confusion_matrix(y_true_oh, y_pred_oh, classes=[], save = False, normalize = False):
    # Convert predictions classes from one hot vectors
    y_pred = np.argmax(y_pred_oh, axis = 1)

    # Convert validation observations from one hot vectors
    y_true = np.argmax(y_true_oh, axis = 1)

    # compute the confusion matrix
    confusion_mtx = confusion_matrix(y_true, y_pred)

    plot_confusion_matrix_core(confusion_mtx, classes, normalize, save)


def plot_confusion_matrix_clean(y_true, y_pred, classes=[], save = False, normalize = False):

    # compute the confusion matrix
    confusion_mtx = confusion_matrix(y_true, y_pred)

    plot_confusion_matrix_core(confusion_mtx, classes, normalize, save)

def print_loss_and_accuracy_from_score(score):
    print('Test loss:', score[0])
    print('Test accuracy:', score[1])


def print_all_scores(precision, recall, accuracy, f1):
    print('Precision (specificity): {0:.3f}%'.format(precision * 100))
    print('Recall (sensitivity): {0:.3f}%'.format(recall * 100))
    print('Accuracy: {0:.3f}%'.format(accuracy * 100))
    print('F1: {0:.3f}%'.format(f1 * 100))

def print_scores(scores):
    """
    Assumes scores is a 4-tuple with the elements (precision, recall, accuracy, f1) in that order.

    ===== Usage =====
    >> y_pred = model.predict(x_val)
    >> scores = get_scores_oh(y_true, y_pred)
    >> print_scores(scores)
    """
    print_all_scores(scores[0], scores[1], scores[2], scores[3])

def get_scores(y_true, y_pred):
    """
    Returns the precision (specificity), recall (sensitivity), accuracy, and f1 scores (in that order).

    It assumes the given vectors are normal 1-D numpy arrays or python lists.
    """
    precision = precision_score(y_true, y_pred, average='macro')
    recall = recall_score(y_true, y_pred, average='macro')
    accuracy = len(np.flatnonzero(y_true == y_pred)) / len(y_true)
    f1 = f1_score(y_true, y_pred, average='macro')
    return (precision, recall, accuracy, f1)

def get_scores_oh(y_true_oh, y_pred_oh):
    """
    Returns the precision (specificity), recall (sensitivity), accuracy, and f1 scores (in that order).

    It assumes the given vectors are one-hot encoded and decodes them.
    """
    y_true = np.argmax(y_true_oh, axis=1)
    y_pred = np.argmax(y_pred_oh, axis=1)

    return get_scores(y_true, y_pred)


def accuracy_score(y_true_oh, y_pred_oh):
    """
    Calculates the accuracy score of one-hot encoded vectors.
    y_true_oh are the ground truths.
    y_pred_oh are the predictions.
    """
    y_true = np.argmax(y_true_oh, axis=1)
    y_pred = np.argmax(y_pred_oh, axis=1)

    assert len(y_true) == len(y_pred)

    y_total = len(y_pred)

    tp_tn = len(np.flatnonzero(y_true == y_pred))

    return tp_tn / y_total
