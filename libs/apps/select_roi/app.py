# import the necessary packages
import argparse
import cv2
import os
import json
import numpy as np

# initialize the list of reference points and boolean indicating
# whether cropping is being performed or not
refPt = []
selection_closed = False
saves = 0

LINE_COLOR = (255,0,0)
SAVES_FILENAME = 'selections.json'


def click_and_crop(event, x, y, flags, param):
    # grab references to the global variables
    global refPt, selection_closed, image, clone

    # If the right button is clicked clear the reference vector
    if event == cv2.EVENT_RBUTTONUP:
        refPt = []

    # check to see if the left mouse button was released
    elif event == cv2.EVENT_LBUTTONUP:
        if selection_closed:
            refPt = []
            image = clone.copy()
            cv2.imshow("image", image)
            selection_closed = False
        # record the ending (x, y) coordinates and indicate that
        # the cropping operation is finished
        refPt.append((x, y))

        ref_len = len(refPt)

        # draw a line from the previous refPt to the current one
        if ref_len > 0:
            cv2.line(image, refPt[ref_len-2], refPt[ref_len-1], LINE_COLOR, 2)
        cv2.imshow("image", image)


def close_selection():
    global refPt, selection_closed

    p1 = refPt[0]
    p2 = refPt[len(refPt)-1]

    cv2.line(image, p1, p2, LINE_COLOR, 2)
    cv2.imshow("image", image)
    selection_closed = True

def parse_args():
    # construct the argument parser and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--image", required=True, help="Path to the image")
    args = vars(ap.parse_args())
    return args

def remove_selections_file():
    if os.path.exists(SAVES_FILENAME):
        os.remove(SAVES_FILENAME)

def save_selection_to_file():
    global saves

    if not os.path.exists(SAVES_FILENAME):
        content = []
        with open(SAVES_FILENAME, 'w') as f:
            f.write(json.dumps(content))

    with open(SAVES_FILENAME, 'r') as f:
        content = json.loads(f.read())
        content.append(np.array(refPt).tolist())
        saves += 1

    with open(SAVES_FILENAME, 'w') as f:
        f.write(json.dumps(content))

def run_app_loop():
    global refPt, image
    while True:
        # display the image and wait for a keypress
        cv2.imshow("image", image)
        key = cv2.waitKey(1) & 0xFF

        # if the 'c' key is pressed, close the selection
        if key == ord('c'):
            close_selection()
        elif key == ord('s'):
            print('saving selection to file\nselection data: {}\n'.format(refPt))
            save_selection_to_file()
            print('{} total saves.'.format(saves))
        # if the 'l' key is pressed, log the selected region to the console
        elif key == ord("r"):
            print(refPt)
        # if the 'c' key is pressed, break from the loop
        elif key == ord("q"):
            break
    # close all open windows
    cv2.destroyAllWindows()

def help():
    print('showing help')

def main_app():
    global image, clone

    args = parse_args()

    img_name = args['image']

    # load the image, clone it, and setup the mouse callback function
    image = cv2.imread(img_name)
    clone = image.copy()
    cv2.namedWindow('image')
    cv2.setMouseCallback('image', click_and_crop)

    # remove previous selections file
    remove_selections_file()

    run_app_loop()


if __name__ == '__main__':
    main_app()
