# ===========================
# filename: patches.py
# author: John Faucett
# date: 2018-04-01
# description: This file contains functions which generate and filter patches from selected ROIs within an image.
#              It has no functions for rendering or displaying these patches.
#              See: render.py for rendering and display functions
# ===========================
import sys
sys.path.append('../')

import os
import cv2
import pandas as pd
import numpy as np
import math
import random
import shortuuid
from shapely.geometry import Polygon
import libs.bounding_box as lbb
import libs.utils as utils


def generate_patches(img, bounding_box, patch_size=64, patch_overlap=0.5, logger=None):
    """
    Take in an image and a bounding box. From this extract patches of `patch_size`
    from the bounded region of the image. Patches overlap by `patch_overlap` amount in the x and y direction.

    This function makes a couple of assumptions.
    1. It assumes the patches will be square, i.e. a given patch_size of 32 will generate patches 32x32 pixels.
    2. It assumes the bounding box can enclose some patches i.e. that the bounding_box is not smaller than the patch_size.
    3. It assumes 0 < patch_overlap <= 1. It will throw an Exception if this is not the case.

    This function fail when passed parameters that would result in patches of different sizes.
    This mainly means that if you have a patch_size of 32 you should use a patch_overlap which results in factors of 32.
    For example patch_overlaps of 0.5, 0.25, or 0.125 all result in incremental step movements of 16, 8, and 4 which
    never jump outside the bounding box (because they are factors of the patch_size of 32).

    It returns a tuple of two elements, the element is a vector containing all the generated patch images.
    The second element is a vector containing the top-left pixel position of the patch image in the original image.

    Complexity: O(N*M) i.e. O(N^2), where N is the number of rows of height patch_size in the bounding box, and M is the number
    of columns of width patch_size in the bounding box.

    Finally, it accepts a logger argument which logs which patches get added or rejected and some info as to why.

    >>> img = cv2.imread('./image.jpg')
    >>> bb = bounding_box.generate_bounding_box(img, roi, patch_size = 64)
    >>> patches, points = generate_patches(img, bb, patch_size=64, patch_overlap=0.5)
    """

    # store shortcuts to the parameters
    bb = bounding_box
    ps = patch_size
    po = patch_overlap

    # ensure we are using a correct patch_overlap range
    if po <= 0 or po > 1:
        raise Exception('patch_overlap must be a value greater than 0 and less than or equal to 1.')

    # store the original top left x and y coordinates of the bounding box
    xorig = bb[0][0]
    yorig = bb[0][1]

    # crop out the region of the image enclosed by the bounding box
    crop_img = img[bb[0][1]:bb[1][1], bb[0][0]:bb[1][0]]
    h, w, _ = crop_img.shape

    # ensure that our bounding box is correctly sized to fit the patch_size
    step = int(ps * po)
    assert(h % step == 0)
    assert(w % step == 0)

    # figure out how many rows and columns we need to iterate over.
    # the rows are just the height of the bounded region divided by the step size.
    # the columns are the width of the bounding region divided by the step size
    rows = int(h / step)
    cols = int(w / step)

    # create the lists we will fill with data
    points = []
    patches = []

    # we run through the image by the row and then all columns in that row, then we move on
    for j in range(rows):

        # the top and bottom y coordinates of the patches for this row
        y = int(j * step)
        y2 = int(y + patch_size)

        for i in range(cols):

            # the left and right x coordinates of the current patch
            x = int(i * step)
            x2 = int(x + patch_size)

            # grab out
            r = img[(yorig + y):(yorig + y2), (xorig + x):(xorig + x2)]

            # store a reference to the top left corner of the patch in the original image for later reference
            rp = np.array([xorig + x, yorig + y])

            # if the patch overlaps such that it is less than the patch size then drop it
            ry, rx, _ = r.shape
            if rx != ps or ry != ps:
                if logger:
                    logger.info('at {}, {}x{} dropped, not patch_size {}x{}'.format(rp, rx,ry,ps,ps))
                continue
            else:
                if logger:
                    logger.info('at {}, patch {}x{} added.'.format(rp,rx,ry))
                points.append(rp)
                patches.append(r)

    return (patches, points)


def filter_patches(patches, roi, points, threshold = 0.5, logger = None):
    """
    Take a vector of patches, an original ROI, and a vector of points, then figure out how many of the patches polygonal area
    overlap's within the threshold value of the ROI Polygon.

    For example, with a generated list of patches from a bounding box region some may overlap only 20% with the ROI. If the
    threshold value is save 0.5 (i.e. 50%) then these patches will be thrown away.

    Returns filtered patches and points vectors.

    >>> patches, points = generate_patches(img, bounding_box)
    >>> filtered_patches, filtered_points = filter_patches(patches, roi, points, threshold= 0.5)
    """
    # store some shortcuts
    t = threshold
    v = roi

    # create a polygon out of the given ROI
    p1 = Polygon(v)

    # take patch size from the first patch
    ps,_,_ = patches[0].shape

    # create our lists we will return from this function
    filtered_patches = []
    filtered_points = []

    for idx in range(len(points)):

        # get the top-left coordinates for this patch
        p = points[idx]

        # create a list of points which describe the outline of the patch
        p2_raw = [p.tolist(), [p[0], p[1]+ps], [p[0]+ps, p[1]+ps], [p[0]+ps, p[1]]]

        # create a polygon for this patch
        p2 = Polygon(p2_raw)

        # check to see if the patch polygon intersects the ROI polygon at all, if not continue to the next patch
        if not p2.intersects(p1):
            continue

        # NOTE: we must ensure polygons are valid!
        # MATLAB appears to insert a point before the closing point of a contour which is not part of the selected ROI
        # this can often create invalid polygons i.e. self-intersecting polygons.
        # In order to catch this error we force both polygons to be valid
        # if they are not valid then we can look at the image, inspect its selected contours, and remove the violating point.
        # Then we can proceed onwards to call this function again.
        assert(p2.is_valid)
        assert(p1.is_valid)

        # calculate what percentage of the patch polygon intersects with the ROI polygon
        iap = p2.intersection(p1).area / p2.area

        # if the intersection percentage equals or exceeds the threshold value, keep this patch
        if iap >= t:
            filtered_points.append(p)
            filtered_patches.append(patches[idx])
            if logger:
                logger.info('{} added, intersection area percentage: {}'.format(p, iap))

    return filtered_patches, filtered_points


def generate_patches_from_df(contours_df, images_df, contours_dir, images_dir, dest_dir, patch_size = 64, patch_overlap = 0.5, threshold = 0.5):

    out_df = pd.DataFrame(columns=['image_id', 'contour_id', 'patch_id', 'patch_name', 'x', 'y', 'label_id'])
    out_df_index = 0

    for index, k in contours_df.iterrows():

        # get the image
        image_id = k['image_id']
        target_img = images_df[images_df['id'] == image_id].iloc[0]
        target_img_path = os.path.join(images_dir, target_img['name'])
        img = cv2.imread(target_img_path)

        # get the contour
        kdf = pd.read_csv(os.path.join(contours_dir, k['id'] + '.csv'), header=None, index_col=False)
        kdf.columns = ['x', 'y']
        kv = utils.convert_roi_dataframe_to_vector(kdf)
        bb = lbb.generate_bounding_box(img, kv, patch_size)

        print('[{}] uuid: {}, bbox: {}, label: {}'.format(index, k['id'], lbb.get_bounding_box_range(bb), k['label']))


        patches, points = generate_patches(img, bb, patch_size, patch_overlap)
        filtered_patches, filtered_points = filter_patches(patches, kv, points, threshold)

        if not os.path.exists(dest_dir):
            raise Exception('destination directory: {} does not exist.'.format(dest_dir))

        # Now we have the patches, their locations in the image (points)
        # Its time to save them to a directory
        id_gen = shortuuid.ShortUUID()
        for idx in range(len(patches)):
            p = patches[idx]

            pt = points[idx]
            p_id = id_gen.random(length=16)
            p_img_name = p_id + '.jpg'
            label_id = k['label']
            p_img_path = os.path.join(dest_dir, p_img_name)

            if os.path.exists(p_img_path):
                raise Exception('patch image already exist: {}'.format(p_img_path))

            # save image
            cv2.imwrite(p_img_path, p)

            # save data
            out_df.loc[out_df_index] = [image_id, k['id'], p_id, p_img_name, pt[0], pt[1], label_id]
            out_df_index += 1

    return out_df
