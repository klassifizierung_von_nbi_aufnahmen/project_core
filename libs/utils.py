# ======================
# filename: utils.py
# ======================
#
# The file has some little "utility" functions for things
# like copying files, normalizing data, generating unique ids
# and converting contour objects into numpy vectors

import numpy as np
import os
import shutil
import distutils.dir_util
import distutils.file_util
import uuid

def simple_scale(data):
    """
    Divides the RGB values by 255 to normalize the image down to a range between 0-1.
    This makes the learning alogrithms work much faster.

    >>> simple_scale(np.array([100,50,60]))
    array([0.3921, 0.1960, 0.2352])

    """
    # https://en.wikipedia.org/wiki/Feature_scaling
    return data / 255


def mean_subtraction(data):
    """
    Perform mean subtraction on a data item

    >>> x = [10,15,20]
    >>> mean_subtraction(x)
    [-1.225, 0.0, 1.225]
    """
    data -= np.mean(data)
    data /= np.std(data)
    return data

def remove_and_create_path(target_path):
    """
    Delete a directory named `target_path` and all its contents recursively
    Then create `target_path` anew
    """
    if os.path.exists(target_path):
        shutil.rmtree(target_path)
    os.makedirs(target_path)

def makedirs(dirs=[]):
    for d in dirs:
        os.makedirs(d)

def short_id():
    """
    Generate a ID that is 8 characters long

    >>> short_id()
    'a5d6b758'
    """
    return str(uuid.uuid4())[:8]

def copy_tree(src_dir, dest_dir):
    distutils.dir_util._path_created = {} # must clear cache, bug in distutils
    distutils.dir_util.copy_tree(src_dir, dest_dir)

def copy_file(src_path, dest_path):
    distutils.dir_util._path_created = {} # must clear cache, bug in distutils
    distutils.file_util.copy_file(src_path, dest_path)

def check_is_dir(target_path):
    if not os.path.isdir(target_path):
        raise Exception('{} is not a directory'.format(target_path))

def convert_roi_to_vector(df):
    """
    Convert a pandas DataFrame to a matrix
    Just an alias function for the DataFrame.to_matrix() call
    """
    return df.as_matrix()

def convert_roi_dataframe_to_vector(df):
    """
    Assumes the DataFrame has two columns 'x' and 'y'.

    It returns a Nx2 matrix where each represents a (x,y) pairing of points.
    """
    v = np.array([df['x'], df['y']])
    return v.transpose()
