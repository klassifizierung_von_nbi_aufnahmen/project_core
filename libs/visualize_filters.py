from vis.visualization import visualize_activation, visualize_saliency
from vis.utils import utils
from keras import activations
from matplotlib import pyplot as plt
import numpy as np
import keras
from keras.models import Sequential
from keras.layers import Flatten, Dense, Dropout
from keras.layers import MaxPooling2D, ZeroPadding2D
from keras.layers.convolutional import Convolution2D
from keras.layers.core import Activation
from keras import backend as K
from keras.utils import np_utils



def plot_activation(model, output_layer, class_idx):
    # Utility to search for layer index by name.
    # Alternatively we can specify this as -1 since it corresponds to the last layer.
    layer_idx = utils.find_layer_idx(model, output_layer)

    # Swap softmax with linear
    model.layers[layer_idx].activation = activations.linear
    model = utils.apply_modifications(model)

    # This is the output node we want to maximize.
    img = visualize_activation(model, layer_idx, filter_indices=class_idx, input_range=(0., 1.), tv_weight=15, lp_norm_weight=0)
    plt.imshow(img[..., 0])
    plt.axis('off')
    plt.show()


def plot_saliency(model, layer, img):
    inputs = [K.learning_phase()] + model.inputs
    _convout1_f = K.function(inputs, [layer.output])
    def convout1_f(X):
        # The [0] is to disable the training phase flag
        return _convout1_f([0] + [X])

    img_to_visualize = np.expand_dims(img, axis=0)


    _convout1_f = K.function(inputs, [layer.output])

    convolutions = convout1_f(img_to_visualize)
    convolutions = np.squeeze(convolutions)

    print ('Shape of conv:', convolutions.shape)


    n = convolutions.shape[0]
    n = int(np.ceil(np.sqrt(n)))

    # Visualization of each filter of the layer
    fig = plt.figure(figsize=(12,8))
    for i in range(len(convolutions)):
        ax = fig.add_subplot(n,n,i+1)
        ax.imshow(convolutions[i], cmap='gray')
