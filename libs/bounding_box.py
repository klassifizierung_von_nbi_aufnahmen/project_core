# ======================
# filename: bounding_box.py
# ======================
#
# This file contains functions for generating a bounding box around ROIs
# It also has functions for getting the size of a bounding box and
# computing the center point of a bounding box.

import numpy as np

def generate_bounding_box(img, roi, patch_size = 64):
    """
    This functions takes an image, ROI, and desired patch_size and creates a bounding box enclosing the ROI
    which is perfectly sized for the desired patch_size.

    The returned bounding box is a 2x2 array with row 1 representing the (x,y) point in the upper left corner of the bounding box.
    The second row of the 2x2 represents the (x,y) coordinates of the lower right hand corner of the bounding box.
    """

    # here we get the height (h), width (w) of the image and the x,y coordinates of the ROI.
    h, w, _ = img.shape
    ps = patch_size
    x = roi.T[0]
    y = roi.T[1]

    # find the min and max values for the ROI within the image.
    x1 = np.min(x)
    x2 = np.max(x)
    y1 = np.min(y)
    y2 = np.max(y)

    # find the x and y ranges
    xrange = x2 - x1
    yrange = y2 - y1

    # figure out how much we need to pad the x and y axes in order to make our patches perfectly fit within the bounding box.
    xneeded = ps - (xrange % ps)
    yneeded = ps - (yrange % ps)

    # if we need more pixels than our image has on the x-axis to the right,
    # then see if we can scoot the bounding box (BB) a little bit to the right move its leftward boundary backwards
    if x2 + xneeded >= w:
        overlap = (x2 + xneeded) - w
        can_add = (xneeded - overlap)
        x2 += can_add
        x1 -= overlap
    else:
        x2 += xneeded

    # same as above but for the y-axis
    if y2 + yneeded >= h:
        overlap = (y2 + yneeded) - h
        can_add = (yneeded - overlap)
        y2 += can_add
        y1 -= overlap
    else:
        y2 += yneeded

    # ensure that our bottom and right edges don't extend beyond the images - should be guaranteed at this point, but assert anyway.
    assert(y2 <= h)
    assert(x2 <= w)

    # Note: sometimes a user may select a region that is the entire width or height of the image
    # if this is the case we could have created a bounding box larger than the underlying image
    # this means that we could have negative values for x1 or y1
    # If this is the case we will just scrink our bounding box by one patch_size in that dimension to accomodate

    if x1 < 0:
        xneg = np.abs(x1)
        x1 = 0
        x2 -= (ps - xneg)
    if y1 < 0:
        yneg = np.abs(y1)
        nps = int(yneg / ps)
        y1 = 0
        if nps == 0:
            y2 -= (ps - yneg)
        else:
            yneg -= (nps * ps)
            y2 -= (ps - yneg)


    # ensure a perfect number of N patches can fit within our bounding box.
    assert((x2 - x1) % ps == 0)
    assert((y2 - y1) % ps == 0)

    # store the bounding box in a 2x2 matrix
    box = np.array([[x1,y1],[x2,y2]])

    return box

def get_bounding_box_range(bounding_box):
    """
    Take in a bounding box and return its width and height as a 2-tuple (width, height).
    """
    x_range = bounding_box[1][0] - bounding_box[0][0]
    y_range = bounding_box[1][1] - bounding_box[0][1]
    return (x_range, y_range)

def get_center(bounding_box):
    """
    Takes a bounding box and returns the center point inside that bounding box.
    Useful for getting the point at which to draw something such as text, label or number inside an ROI or patch.
    """
    w, h = get_bounding_box_range(bounding_box)
    pt = bounding_box[0]
    x_center = int(pt[0] + (w/2))
    y_center = int(pt[1] + (h/2))
    return (x_center, y_center)
