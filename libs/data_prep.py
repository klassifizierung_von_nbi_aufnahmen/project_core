# ======================
# filename: data_prep.py
# ======================
#
# This file contains code for preparing to do each experiment
#
# It has functions for checking if data contains NaNs and functions to shuffle
# arrays so that they are random enough to train models well
#
# It has lots of debugging and logging utilities that let you inspect every phase
# of the data extraction pipeline

# The most important functions are:
# 1.  show_contours_for_experiment : show which ROIs were selected and which were removed when starting a new experiment
# 2.  log_patch_data_reader(2) : logs all patches inside their original image as they are created and ensures data correctness
# 3. setup_patches_for, filter_highlight_patches_for : create patches and filter out bad quality ones
# 4. setup_experiment, setup_component, log_component_info : setup a new experiment, debug test/train/val data readers

import sys
sys.path.append('../')
import libs.utils as utils
import libs.render as render
import libs.patch_tools as patch_tools
import libs.bounding_box as lbb
import libs.cleaning as cleaning
import os
import cv2
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


def shuffle_two_arrays(a,b):
    """
    Randomly shuffle two arrays and return the shuffled versions
    """
    n = a.shape[0]
    idxs = np.arange(n)
    np.random.shuffle(idxs)
    np.random.shuffle(idxs)
    np.random.shuffle(idxs)

    return (a[idxs], b[idxs])

def shuffle_indices(n):
    """
    Return shuffled indices for an array
    Useful if you have an array and want a randomized version of its contents

    >>> a = np.array([1,2,3,4])
    >>> k = len(a)
    >>> idxs = shuffle_indices(k)
    >>> a[idxs]
    np.array([4,2,1,3])
    """
    idxs = np.arange(n)
    np.random.shuffle(idxs)
    np.random.shuffle(idxs)
    np.random.shuffle(idxs)
    np.random.shuffle(idxs)

    return idxs

def check_for_nans(data):
    """
    Iterate over an array of matrices and ensure no item is a NaN - anywhere.
    """
    for idx in range(len(data)):
        item = data[idx]
        if np.isnan(item).any():
            raise Exception('Data Point at [] contains nan values.'.format(idx))

def setup_experiment(source_dir, exp_id):
    """
    Create a directory and copy needed files for starting a new experiment
    """
    # ensure the source directory exists
    utils.check_is_dir(source_dir)

    # create the directory for our experiment (delete and recreate if it already exists)
    utils.remove_and_create_path(exp_id)

    # copy the data we want (everything but debugging stuff)
    utils.copy_tree(os.path.join(source_dir, 'images'), os.path.join(exp_id, 'images'))
    utils.copy_tree(os.path.join(source_dir, 'contours'), os.path.join(exp_id, 'contours'))
    utils.copy_file(os.path.join(source_dir, 'images.csv'), os.path.join(exp_id, 'images.csv'))
    utils.copy_file(os.path.join(source_dir, 'contours.csv'), os.path.join(exp_id, 'contours.csv'))

    utils.makedirs([os.path.join(exp_id, 'log')])


def setup_component(source_dir, component_df, component_name):
    # shortcuf refs
    df = component_df

    # ensure source dir and others exists
    src_images = os.path.join(source_dir, 'images')
    src_contours = os.path.join(source_dir, 'contours')
    utils.check_is_dir(source_dir)
    utils.check_is_dir(src_images)
    utils.check_is_dir(src_contours)

    # create our component directories
    component_dir = os.path.join(source_dir, component_name)
    log_dir = os.path.join(component_dir, 'log')
    images_dir = os.path.join(component_dir, 'images')
    contours_dir = os.path.join(component_dir, 'contours')

    utils.remove_and_create_path(component_dir)
    utils.makedirs([log_dir, images_dir, contours_dir])

    images_df = pd.read_csv(os.path.join(source_dir, 'images.csv'))
    image_ids = np.unique(df['image_id'].values)

    images_df = images_df[images_df['id'].isin(image_ids.tolist())].reset_index(drop=True)

    for image_id in image_ids:
        image_name = '{}.jpg'.format(image_id)
        utils.copy_file(os.path.join(src_images, image_name), os.path.join(images_dir, image_name))

    # Copy the contours we need
    for idx, row in df.iterrows():
        contour_name = row['name']
        utils.copy_file(os.path.join(src_contours, contour_name), os.path.join(contours_dir, contour_name))

    # Save the contours dataframe
    df.to_csv(os.path.join(component_dir, 'contours.csv'), index=False)
    images_df.to_csv(os.path.join(component_dir, 'images.csv'), index=False)

def log_component_info(source_dir):

    utils.check_is_dir(source_dir)

    images_dir = os.path.join(source_dir, 'images')
    contours_dir = os.path.join(source_dir, 'contours')
    log_dir = os.path.join(source_dir, 'log')
    info_dir = os.path.join(log_dir, 'info')
    roi_df = pd.read_csv(os.path.join(source_dir, 'contours.csv'))

    utils.remove_and_create_path(info_dir)

    for idx, row in roi_df.iterrows():
        img_id = row['image_id']
        img_name = '{}.jpg'.format(img_id)
        img = cv2.imread(os.path.join(images_dir, img_name))

        roi_name = row['name']
        roi_label = render.get_label_from_id(row['label'], start_index=1)
        roi_df = pd.read_csv(os.path.join(contours_dir, roi_name), header=None, index_col=False)
        roi = utils.convert_roi_to_vector(roi_df)

        img = render.draw_roi_on_image(img, roi)
        img = render.draw_text_on_image_center(img, roi_label)
        out_img_name = '{}_{}.jpg'.format(img_id, row['id'])
        cv2.imwrite(os.path.join(info_dir, out_img_name), img)


def setup_patches_for(component_dir, patch_size = 64, patch_overlap = 0.5, threshold = 0.5):
    utils.check_is_dir(component_dir)

    # Refs
    roi_df = pd.read_csv(os.path.join(component_dir, 'contours.csv'))
    img_df = pd.read_csv(os.path.join(component_dir, 'images.csv'))
    roi_dir = os.path.join(component_dir, 'contours')
    img_dir = os.path.join(component_dir, 'images')
    patches_dir = os.path.join(component_dir, 'patches')
    patches_log_dir = os.path.join(component_dir, 'log', 'setup_patches')

    # clear and create the patches directory
    utils.remove_and_create_path(patches_dir)
    utils.remove_and_create_path(patches_log_dir)

    # create all my patches
    # 1. create images of the patches that have been eliminated
    # 2. create images of the patches that have been generated
    # 3. store the dataframe
    out_df = pd.DataFrame(columns=['image_id', 'contour_id', 'patch_id', 'patch_name', 'x', 'y', 'label_id'])
    out_df_index = 0

    for idx, k in roi_df.iterrows():

        # fetch the image
        image_id = k['image_id']
        target_img = img_df[img_df['id'] == image_id].iloc[0]
        target_img_path = os.path.join(img_dir, target_img['name'])
        img = cv2.imread(target_img_path)

        # fetch the contour
        kdf = pd.read_csv(os.path.join(roi_dir, k['id'] + '.csv'), header=None, index_col=False)
        kdf.columns = ['x', 'y']
        kv = utils.convert_roi_dataframe_to_vector(kdf)
        bb = lbb.generate_bounding_box(img, kv, patch_size)

        # GENERATE PATCHES
        patches, points = patch_tools.generate_patches(img, bb, patch_size, patch_overlap)

        # debug image : before filtering out patches
        debug_filename = '{}_{}_no_filter.jpg'.format(image_id, k['id'])
        img2 = img.copy()
        img2 = render.draw_bounding_box_on_image(img2, bb)
        img2 = render.draw_roi_on_image(img2, kv, patch_size)
        img2 = render.draw_patches_on_image(img2, points, patch_size)
        img2 = render.draw_text_on_image_center(img2, render.get_label_from_id(k['label'], start_index=1))

        cv2.imwrite(os.path.join(patches_log_dir, debug_filename), img2)

        # FILTER OUTLIER PATCHES
        filtered_patches, filtered_points = patch_tools.filter_patches(patches, kv, points, threshold)

        debug_filename = '{}_{}_filter.jpg'.format(image_id, k['id'])
        img2 = img.copy()
        img2 = render.draw_bounding_box_on_image(img2, bb)
        img2 = render.draw_roi_on_image(img2, kv, patch_size)
        img2 = render.draw_patches_on_image(img2, filtered_points, patch_size)
        img2 = render.draw_text_on_image_center(img2, render.get_label_from_id(k['label'], start_index=1))

        cv2.imwrite(os.path.join(patches_log_dir, debug_filename), img2)

        # Now we have the patches, their locations in the image (points)
        # Its time to save them to a directory
        for idx in range(len(filtered_patches)):
            p = filtered_patches[idx]

            pt = filtered_points[idx]

            p_id = utils.short_id()
            p_img_name = '{}.jpg'.format(p_id)
            p_img_path = os.path.join(patches_dir, p_img_name)
            label_id = k['label']
            counter = 0
            while True:
                if counter >= 5:
                    raise Exception('unable to uniquely generate an ID for patch. Increase counter limit.')

                if os.path.exists(p_img_path):
                    p_id = utils.short_id()
                    p_img_name = '{}.jpg'.format(p_id)
                    p_img_path = os.path.join(patches_dir, p_img_name)
                    counter += 1
                else:
                    break


            # save image
            cv2.imwrite(p_img_path, p)

            # save data
            out_df.loc[out_df_index] = [image_id, k['id'], p_id, p_img_name, pt[0], pt[1], label_id]
            out_df_index += 1

    out_df.to_csv(os.path.join(component_dir, 'patches.csv'), index=False)

    return out_df


def filter_highlight_patches_for(source_dir, patch_size = 64, threshold = 1.0):
    utils.check_is_dir(source_dir)

    images_dir = os.path.join(source_dir, 'images')
    contours_dir = os.path.join(source_dir, 'contours')
    patches_dir = os.path.join(source_dir, 'patches')
    filter_log_dir = os.path.join(source_dir, 'log', 'filter_highlight_patches')
    filter_log_dir_p = os.path.join(filter_log_dir, 'patches')
    filter_log_dir_i = os.path.join(filter_log_dir, 'images')

    df = pd.read_csv(os.path.join(source_dir, 'patches.csv'), index_col=False)

    utils.remove_and_create_path(filter_log_dir)
    utils.remove_and_create_path(filter_log_dir_p)
    utils.remove_and_create_path(filter_log_dir_i)

    white_img = cleaning.create_white_image((patch_size, patch_size, 3))

    bad_patches = []
    bad_rois = set()

    for idx, row in df.iterrows():
        img_name = '{}.jpg'.format(row['image_id'])
        img_path = os.path.join(images_dir, img_name)

        patch_id = row['patch_id']
        patch_name = row['patch_name']
        patch_path = os.path.join(patches_dir, patch_name)
        patch_img = cv2.imread(patch_path)

        if cleaning.rmse(patch_img, white_img) <= threshold:
            # we can delete this patch
            bad_patches.append(patch_id)
            bad_rois.add(row['contour_id'])

            # copy patch to patch
            utils.copy_file(patch_path, os.path.join(filter_log_dir_p, patch_name))

    for roi in bad_rois:
        tmp_df = df[df['contour_id'] == roi]
        tmp_df = tmp_df[tmp_df['patch_id'].isin(bad_patches)]
        image_id = tmp_df['image_id'].values[0]
        image_name = '{}.jpg'.format(image_id)
        debug_filename = '{}_{}.jpg'.format(image_id, roi)
        label_id = tmp_df['label_id'].values[0]

        # get all filtered points
        px = tmp_df['x'].values
        py = tmp_df['y'].values
        points = np.array([px, py]).transpose()

        # fetch the contour
        kdf = pd.read_csv(os.path.join(contours_dir, roi + '.csv'), header=None, index_col=False)
        kdf.columns = ['x', 'y']
        kv = utils.convert_roi_dataframe_to_vector(kdf)


        # read in the original image
        img2 = cv2.imread(os.path.join(images_dir, image_name))

        # create bounding box
        bb = lbb.generate_bounding_box(img2, kv, patch_size)

        img2 = render.draw_bounding_box_on_image(img2, bb)
        img2 = render.draw_roi_on_image(img2, kv, patch_size)
        img2 = render.draw_patches_on_image(img2, points, patch_size)
        img2 = render.draw_text_on_image_center(img2, render.get_label_from_id(label_id, start_index=1))

        cv2.imwrite(os.path.join(filter_log_dir_i, debug_filename), img2)


    print('bad patches: {}'.format(len(bad_patches)))
    print('before: {}'.format(len(df)))
    df_cleaned = df[~df.patch_id.isin(bad_patches)]
    df_cleaned.reset_index(drop=True)
    print('cleaned: {}'.format(len(df_cleaned)))

    # Save the cleaned dataframe
    df_cleaned.to_csv(os.path.join(source_dir, 'patches_cleaned.csv'), index=False)

    return df_cleaned

def log_patch_data_reader(root_dir, pdr):

    patches_dir = pdr.get_root

    log_dir = os.path.join(root_dir, 'log', 'patch_data_reader')

    # ensure we have the directory to log the files into
    utils.remove_and_create_path(log_dir)

    image_ids = np.unique(pdr.df['image_id'].values)

    for image_id in image_ids:

        image_name = '{}.jpg'.format(image_id)

        idxs = np.flatnonzero(pdr.df['image_id'] == image_id)
        subset_df = pdr.df[pdr.df['image_id'] == image_id].reset_index(drop=True)
        #display(subset_df)
        patch_images = pdr.get_images()[idxs]
        patch_labels = pdr.get_labels()[idxs]

        # ensure all our patch data lines up correctly
        assert len(idxs) == len(patch_images) == len(patch_labels) == len(subset_df)

        img = cv2.imread(os.path.join(root_dir, 'images', image_name))

        for idx in range(len(idxs)):
            new_img = np.zeros(img.shape)
            patch_img = patch_images[idx]
            patch_label = patch_labels[idx]
            patch_row = subset_df.loc[idx]

            (pw, ph, _) = patch_img.shape

            assert patch_label == patch_row['label_id']

            px = patch_row['x']
            py = patch_row['y']

            # write label on patch_img
            new_pi = patch_img.copy()
            font = cv2.FONT_HERSHEY_SIMPLEX
            new_pi = cv2.putText(new_pi,str(patch_label),(10,10), font, 1, (0,255,0),1, cv2.LINE_AA)

            img[py:(py+pw), px:(px+ph)] = new_pi


        cv2.imwrite(os.path.join(log_dir, image_name), img)

def log_patch_data_reader2(root_dir, pdr):

    patches_dir = pdr.get_root
    ps = pdr.patch_size

    log_dir = os.path.join(root_dir, 'log', 'patch_data_reader')

    # ensure we have the directory to log the files into
    utils.remove_and_create_path(log_dir)

    image_ids = np.unique(pdr.df['image_id'].values)

    for image_id in image_ids:

        image_name = '{}.jpg'.format(image_id)

        idxs = np.flatnonzero(pdr.df['image_id'] == image_id)
        subset_df = pdr.df[pdr.df['image_id'] == image_id].reset_index(drop=True)
        #display(subset_df)
        patch_images = pdr.get_images()[idxs]
        patch_labels = pdr.get_labels()[idxs]

        # ensure all our patch data lines up correctly
        assert len(idxs) == len(patch_images) == len(patch_labels) == len(subset_df)

        img = cv2.imread(os.path.join(root_dir, 'images', image_name))

        for idx in range(len(idxs)):
            new_img = np.zeros(img.shape)
            patch_img = patch_images[idx]
            patch_label = patch_labels[idx]
            patch_row = subset_df.loc[idx]

            (pw, ph, _) = patch_img.shape

            assert patch_label == patch_row['label_id']

            px = patch_row['x']
            py = patch_row['y']

            # write label on patch_img
            new_pi = patch_img.copy()
            font = cv2.FONT_HERSHEY_SIMPLEX
            color = (0,0,255)
            border_size = 2
            img = cv2.line(img, (px, py), (px + ps, py), color, 2)
            img = cv2.line(img, (px + ps, py), (px + ps, py + ps), color, border_size)
            img = cv2.line(img, (px + ps, py + ps), (px, py + ps), color, border_size)
            img = cv2.line(img, (px, py + ps), (px, py), color, border_size)
            img = cv2.putText(img, str(patch_label), (px + int(ps/2) - 6,py + int(ps/2) + 6), font, 0.75, (255,0,0),1, cv2.LINE_AA)


            #img_overlay = cv2.circle(img, (px, py), 5, (0,255,0), -1)

            #img[py:(py+pw), px:(px+ph)] = new_pi


        cv2.imwrite(os.path.join(log_dir, image_name), img)

def show_contours_for_experiment(exp_dir, roi_df, roi_exp_df):

    log_dir = os.path.join(exp_dir, 'log')
    log_dir_elim = os.path.join(log_dir, 'eliminated_contours')
    log_dir_kept = os.path.join(log_dir, 'experiment_contours')

    elim_df = roi_df[~roi_df['id'].isin(roi_exp_df['id'])].reset_index(drop=True)

    utils.remove_and_create_path(log_dir_elim)
    utils.remove_and_create_path(log_dir_kept)

    print('total contours: {}'.format(len(roi_df)))
    print('eliminated contours: {}'.format(len(elim_df)))
    print('kept contours: {}'.format(len(roi_exp_df)))

    display(elim_df)

    for idx, row in elim_df.iterrows():
        image_id = row['image_id']
        image_name = '{}.jpg'.format(image_id)
        img_path = os.path.join(exp_dir, 'images', image_name)
        img = cv2.imread(img_path)

        roi_label = render.get_label_from_id(row['label'], start_index=1)
        roi_name = row['name']
        roi_path = os.path.join(exp_dir, 'contours', roi_name)
        roi_df = pd.read_csv(roi_path, header=None)
        roi = utils.convert_roi_to_vector(roi_df)

        img = render.draw_roi_on_image(img, roi)
        img = render.draw_text_on_image_center(img, roi_label)

        out_name = '{}_{}.jpg'.format(image_id, roi_name)

        cv2.imwrite(os.path.join(log_dir_elim, out_name), img)

    for idx, row in roi_exp_df.iterrows():
        image_id = row['image_id']
        image_name = '{}.jpg'.format(row['image_id'])
        img_path = os.path.join(exp_dir, 'images', image_name)
        img = cv2.imread(img_path)

        roi_label = render.get_label_from_id(row['label'], start_index=1)
        roi_name = row['name']
        roi_path = os.path.join(exp_dir, 'contours', roi_name)
        roi_df = pd.read_csv(roi_path, header=None)
        roi = utils.convert_roi_to_vector(roi_df)

        img = render.draw_roi_on_image(img, roi)
        img = render.draw_text_on_image_center(img, roi_label)

        out_name = '{}_{}.jpg'.format(image_id, roi_name)

        cv2.imwrite(os.path.join(log_dir_kept, out_name), img)
