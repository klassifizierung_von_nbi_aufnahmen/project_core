# ======================
# filename: cleaning.py
# ======================
#
# This file contains functions for "cleaning up" images, ROIs and patches.
#
# It provides functions which can use an image histogram or the
# root mean squared difference between two images to check if an image is too specular.
#
# It also provides a higher level function `is_image_too_specular` specifically for this task.

import numpy as np
import cv2

def create_white_image(shape):
    """
    Create a pure white image. Useful for comparing two images to see if one
    is so specular it is within a threshold value of the white image.
    """
    return np.ones(shape) * 255

def rmse(img1, img2):
    """
    Compute the RMSE (Root Mean Squared) Difference between two images.

    >>> im1 = cv2.imread('a.jpg')
    >>> im2 = cv2.imread('b.jpg')
    >>> rmse(im1, im2)
    0.789
    """
    h,w,d = img1.shape
    rmse = np.sqrt( np.sum(np.power(img1 - img2, 2))) / (h * w)
    return rmse

def hist_percentage_in_range(img, channel=0, start=0, end=256):
    hist = cv2.calcHist([img], [channel], None, [256], [0,256])

    color_value_counts = 0
    for k in range(start, end):
        color_value_counts += hist[k]

    return color_value_counts / np.sum(hist)


def is_image_too_specular(img, comparison_img, threshold=1.0):
    """
    If img is within `threshold` value of the comparison_img then it is considered too specular
    Usually a pure white image is used for the comparison_img.
    """
    if rmse(img, comparison_img) < threshold:
        return True
    else:
        return False
