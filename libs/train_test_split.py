# ======================
# filename: train_test_split.py
# ======================
#
# Implementation of the train, test, validation split function used in the research project
# There are some auxilliary utility functions that serve to support splitting out the data

import numpy as np
import pandas as pd

def counts(values):
    return np.unique(values, return_counts=True)

def select_random_indices(df, class_ids, size=2):
    class_choices = []

    for label_id in class_ids:
        class_idxs = np.flatnonzero(df['label'] == label_id)
        label_idxs = np.random.choice(class_idxs, size=size, replace=False)
        class_choices.append(label_idxs)

    result = np.array(class_choices).flatten()

    assert(len(result) == (len(class_ids) * size))

    return result

def train_test_validation_split(roi_df, classes, samples=2):
    """
    Given a pandas DataFrame `roi_df` containing a list of contours and a classes array (from 1-N)
    create a train, test, validation split of the data where the test and validation splits
    each have N-samples (default = 2) per class.

    Returns 3 DataFrame objects holding the train, test, and validation frames.

    Example:

    train, test, val = train_test_validation_split(df, [1,2,3], samples=3)
    # now test and val each have 9 rows b/c they each have 3 samples from each class (1-3)
    """
    class_ids = np.array(classes)
    labels, label_counts = counts(roi_df['label'].values)

    # ensure all the labels in our dataframe line up with the classes we have given
    assert np.all(labels == class_ids)

    # view summary information
    print('n: {}'.format(len(roi_df)))
    print('distribution: {} {}'.format(labels, label_counts))
    print('classes: {}, samples_per_class: {}'.format(class_ids, samples))

    test_idxs = select_random_indices(roi_df, class_ids, size=samples)
    test_df = roi_df[roi_df.index.isin(test_idxs)].reset_index(drop=True)

    train_df = roi_df[~roi_df.index.isin(test_idxs)].reset_index(drop=True)

    val_idxs = select_random_indices(train_df, class_ids, size=samples)
    val_df = train_df[train_df.index.isin(val_idxs)].reset_index(drop=True)

    #print('Test DataFrame')
    #display(test_df)
    #print('Validation dataFrame')
    #display(val_df)

    train_df = train_df[~train_df.index.isin(val_idxs)].reset_index()

    print('train-n: {}'.format(len(train_df)))
    print('test-n: {}'.format(len(test_df)))
    print('val-n: {}'.format(len(val_df)))

    return (train_df, test_df, val_df)
