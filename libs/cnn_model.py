# ======================
# filename: cnn_model.py
# ======================
#
# An early stage file that can be largely ignored unless you want to look at eda directory stuff
#
from keras.utils.np_utils import to_categorical # convert to one-hot-encoding
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Conv2D, MaxPool2D
from keras.optimizers import RMSprop
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import ReduceLROnPlateau
import numpy as np


def build_cnn_model(num_classes, input_shape=(64,64,3), layer2=False):
    model = Sequential()

    # padding = 'same' ; this means we are zero padding the image
    # kernel_size (5,5) ; this is automatically extended by keras to the depth of the image so the filter is really (5x5x3)

    model.add(Conv2D(filters = 32, kernel_size = (5,5),padding = 'Same', activation ='relu', input_shape = input_shape))
    # output => 64x64x32

    model.add(Conv2D(filters = 64, kernel_size = (5,5), padding = 'Same', activation ='relu'))
    # output => 64x64x64

    model.add(MaxPool2D(pool_size=(2,2)))
    # downsampled output => 32x32x64

    model.add(Dropout(0.25)) # randomly kill some neurons to increase generalizability

    if layer2:
        model.add(Conv2D(filters = 64, kernel_size = (3,3), padding = 'Same', activation ='relu'))
        # output => 64x64x64

        model.add(Conv2D(filters = 64, kernel_size = (3,3),padding = 'Same', activation ='relu'))

        model.add(MaxPool2D(pool_size=(2,2), strides=(2,2)))
        model.add(Dropout(0.25))


    model.add(Flatten())
    model.add(Dense(256, activation = "relu"))
    model.add(Dropout(0.5))
    model.add(Dense(num_classes, activation = "softmax"))

    return model

def simple_cnn_model(num_classes, input_shape=(64,64,3)):
    model = Sequential()

    # padding = 'same' ; this means we are zero padding the image
    # kernel_size (5,5) ; this is automatically extended by keras to the depth of the image so the filter is really (5x5x3)

    model.add(Conv2D(filters = 64, kernel_size = (5,5),padding = 'Same', activation ='relu', input_shape = input_shape))
    # output => 64x64x32

    model.add(Conv2D(filters = 64, kernel_size = (5,5), padding = 'Same', activation ='relu'))
    # output => 64x64x64

    model.add(MaxPool2D(pool_size=(2,2)))
    # downsampled output => 32x32x64

    model.add(Dropout(0.25)) # randomly kill some neurons to increase generalizability

    model.add(Conv2D(filters = 32, kernel_size = (3,3), padding = 'Same', activation ='relu'))
    # output => 32x32x32

    model.add(Conv2D(filters = 32, kernel_size = (3,3),padding = 'Same', activation ='relu'))
    # output => 32x32x32

    model.add(MaxPool2D(pool_size=(2,2), strides=(2,2)))
    # output => 16x16x32

    model.add(Dropout(0.25))


    model.add(Flatten())
    model.add(Dense(256, activation = "relu"))
    # output => 256x1
    model.add(Dropout(0.5))
    model.add(Dense(num_classes, activation = "softmax"))
    # output => <num-classes> x 1

    return model


def build_optimizer(learning_rate=0.001):
    optimizer = RMSprop(lr=learning_rate, rho=0.9, epsilon=1e-08, decay=0.0)

def get_learning_rate_reduction():
    return ReduceLROnPlateau(monitor='val_acc', patience=3, verbose=1, factor=0.5, min_lr=0.00001)
