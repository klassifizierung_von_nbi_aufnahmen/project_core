# ======================
# filename: data_prep.py
# ======================
#
# This file contains code for rendering images, drawing contours and in general dealing with
# well, rendering of images using OpenCV.

import sys
sys.path.append('../')

import numpy as np
import cv2
import matplotlib.pyplot as plt
import libs.bounding_box as bounding_box
from importlib import reload
from sklearn.metrics import confusion_matrix
import itertools

DEFAULT_CONTOUR_COLOR = (0,255,0)
DEfAULT_BOUNDING_BOX_COLOR = (255,0,0)
DEFAULT_PATCH_OUTLINE_COLOR = (255,0,0)
DEFAULT_HEATMAP_COLORS = [
    (0,255,0),     # benign = green
    (255,165,0),   # papilloma = orange
    (255,0,0),     # dysplasia = red
    (255,0,0),     # carcinoma = red
]

def flip_channels(img):
    """
    By default OpenCV stores images in BGR (Blue-Green-Red) channel order.
    If we render images they will look odd b/c we will be rendering the blue channel as red and vice-versa.

    This function swaps the color channels from a BGR to RGB order in the image matrix.

    >>> img = cv2.imread('./image.jpg')   # img color channels are stored BGR by default
    >>> img2 = flip_channels(img)         # now img2 color channels are stored RGB
    """
    return cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

def draw_roi_on_image(img, roi, color=DEFAULT_CONTOUR_COLOR):
    """
    Draws a ROI (Region of Interest i.e. contour) vector on the given image.
    The color of contour defaults to bright green (0,255,0).

    To override the color give a 3-tuple i.e. red is (255,0,0).

    It returns the new image containing the ROI outline
    """
    return cv2.polylines(img,[roi],True, color, 3)

def draw_bounding_box_on_image(img, bounding_box, color=DEfAULT_BOUNDING_BOX_COLOR):
    """
    Draws the bounding box which encloses a contour (ROI).

    The default color of the bounding box is red (255,0,0).
    It can be override by giving a 3-tuple representing the desired bounding box color.
    """
    p1 = tuple(bounding_box[0])
    p3 = tuple(bounding_box[1])
    return cv2.rectangle(img, p1, p3, color, 3)


def flip_and_plot_image(img):
    """
    Swap the color channels of an image and plot it using matplotlib.

    >>> img = cv2.imread('./image.jpeg')
    >>> flip_and_plot_image(img)
    """
    imgc = flip_channels(img)
    plt.axis('off')
    plt.imshow(imgc)
    plt.show()


def plot_image(img):
    """
    Plots an image using Matplotlib.
    It calls pyplot.show() so the image is immediately displayed.

    >>> img = cv2.imread('./image.png')
    >>> plot_image(img)
    """
    plt.axis('off')
    plt.imshow(img)
    plt.show()

def plot_image_with_roi(img, roi):
    """
    Draws a ROI on top of the image and then plots the new image.
    """
    img = draw_roi_on_image(img, roi)
    plot_image(img)

def plot_image_with_bounding_box(img, bounding_box):
    """
    Draws a bounding box on top of the image and then plots the new image.
    """
    img = draw_bounding_box_on_image(img, bounding_box)
    plot_image(img)

def plot_image_with_roi_and_bounding_box(img, roi, bounding_box):
    """
    Draws an ROI and a surrounding bounding box on the image before plotting it.
    """
    img = draw_roi_on_image(img, roi)
    img = draw_bounding_box_on_image(img, bounding_box)
    plot_image(img)


def draw_patches_on_image(img, points, patch_size = 64, color = DEFAULT_PATCH_OUTLINE_COLOR, border_size=3, alpha=0.2):
    ps = patch_size
    output = img.copy()

    for i in range(len(points)):
        p = points[i]

        # Draw the found lines of the patch box
        img_overlay = cv2.line(output.copy(), (p[0], p[1]), (p[0] + ps, p[1]), color, border_size)
        img_overlay = cv2.line(img_overlay, (p[0] + ps, p[1]), (p[0] + ps, p[1] + ps), color, border_size)
        img_overlay = cv2.line(img_overlay, (p[0] + ps, p[1] + ps), (p[0], p[1] + ps), color, border_size)
        img_overlay = cv2.line(img_overlay, (p[0], p[1] + ps), (p[0], p[1]), color, border_size)

        cv2.addWeighted(img_overlay, alpha, output, 1 - alpha, 0, output)
    return output

def plot_image_with_patches(img, points):
    img = draw_patches_on_image(img, points)
    plot_image(img)

def plot_image_with_roi_and_patches(img, roi, points):
    img = draw_roi_on_image(img, roi)
    img = draw_patches_on_image(img, points)
    plot_image(img)

def has_color_for_each_label(labels, colors):
    values = np.unique(labels)
    for k in values:
        try:
            colors[k]
        except IndexError as ex:
            return False
    return True

def draw_patches_heatmap_on_image(img, points, labels, colors=DEFAULT_HEATMAP_COLORS):
    base_alpha = 0.2
    output = img.copy()
    if not has_color_for_each_label(labels, colors):
        raise Exception('invalid colors array. There must be a color available for each label.')

    for idx in range(len(points)):
        p = points[idx]
        label = labels[idx]
        color = DEFAULT_HEATMAP_COLORS[label]
        alpha = base_alpha * label
        img_overlay = cv2.rectangle(output.copy(), (p[0], p[1]), (p[0] + 64, p[1] + 64), color, -1)
        cv2.addWeighted(img_overlay, alpha, output, 1 - alpha, 0, output)

    return output

def draw_text_on_image_center(img, text):
    h,w, _ = img.shape
    cx = int(w/2)
    cy = int(h/2)
    font = cv2.FONT_HERSHEY_PLAIN
    img = cv2.putText(img, text, (cx, cy), font, 2, (255,0,0), 2, cv2.LINE_AA)
    return img


def get_label_from_id(label_id, start_index = 0):
    label_id = str(label_id)
    if start_index == 0:
        if label_id == '0':
            return 'benign'
        elif label_id == '1':
            return 'papilloma'
        elif label_id == '2':
            return 'dysplasia'
        elif label_id == '3':
            return 'carcinoma'
        else:
            raise Exception('unknown label: {}'.format(label_id))
    elif start_index == 1:
        if label_id == '1':
            return 'benign'
        elif label_id == '2':
            return 'papilloma'
        elif label_id == '3':
            return 'dysplasia'
        elif label_id == '4':
            return 'carcinoma'
        else:
            raise Exception('unknown label: {}'.format(label_id))
    else:
        raise Exception('unregistered start index for labels.')

def draw_image_data_at_index(data_source, index, patch_size = 64, logger = None):
    img         = data_source.image_data[index].copy()
    img         = flip_channels(img)
    img_name    = data_source.image_names[index]
    img_id      = data_source.image_ids[index]
    rois        = data_source.roi_data[index]
    roi_ids     = data_source.roi_ids[index]
    roi_labels  = data_source.roi_labels[index]
    h,w,_ = img.shape
    thickness = (h/1000) + (w/1000)

    for idx in range(len(rois)):
        roi = rois[idx]
        roi_id = roi_ids[idx]
        roi_label = roi_labels[idx]
        bb = bounding_box.generate_bounding_box(img, roi, patch_size)
        cp = bounding_box.get_center(bb)

        # draw the contour
        img = cv2.polylines(img,[roi],True, DEFAULT_CONTOUR_COLOR, int(thickness))

        # draw the contour name
        font = cv2.FONT_HERSHEY_PLAIN
        img = cv2.putText(img, roi_id, (cp[0], cp[1]-int(15*thickness)), font, thickness, (255,0,0), int(thickness), cv2.LINE_AA)

        # draw the contour label
        img = cv2.putText(img, get_label_from_id(roi_label), cp, font, thickness, (255,0,0), int(thickness), cv2.LINE_AA)

    if logger:
        logger.info('image [{}x{}]: {}, rois: [{}], roi_labels: [{}]'.format(w,h, img_id, roi_ids, roi_labels))

    img = cv2.putText(img, img_name, (10, 38), font, 3, (255,0,0), 2, cv2.LINE_AA)
    return img


def plot_patches_per_category_distribution(label_data, title='', num_classes = 4):
    label_ids, label_counts = np.unique(label_data, return_counts=True)
    total_items = np.sum(label_counts)
    columns = ['Category', 'Patch Count', 'Percentage of Total']
    rows = label_ids
    cell_text = []

    for idx in range(num_classes):
        item = [label_ids[idx], label_counts[idx], np.round((label_counts[idx] / total_items)*100, 2) ]
        cell_text.append(item)

    fig, ax = plt.subplots()
    ax.axis('tight')
    ax.axis('off')
    the_table = ax.table(cellText=cell_text,
                         colLabels=columns, loc='center')
    plt.title('Image per Category Distribution: {}'.format(title))
    plt.show()


def plot_training_and_validation_curves(history, save = False):
    # Plot the loss and accuracy curves for training and validation
    fig, ax = plt.subplots(2,1)
    ax[0].plot(history.history['loss'], color='b', label="Training loss")
    ax[0].plot(history.history['val_loss'], color='r', label="validation loss",axes =ax[0])
    legend = ax[0].legend(loc='best', shadow=True)

    ax[1].plot(history.history['acc'], color='b', label="Training accuracy")
    ax[1].plot(history.history['val_acc'], color='r',label="Validation accuracy")
    legend = ax[1].legend(loc='best', shadow=True)

    if save:
        plt.savefig('plot_training_and_validation_curves.png')
    else:
        plt.show()

def setup_confusion_matrix(y_pred, x, y):

    # Convert predictions classes from one hot vectors
    y_pred_classes = np.argmax(y_pred, axis = 1)

    # Convert true data from one hot vectors
    y_true = np.argmax(y,axis = 1)

    # compute the confusion matrix
    return confusion_matrix(y_true, y_pred_classes)


def plot_confusion_matrix(cm, num_classes, normalize=False, title='Confusion matrix', cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """

    classes = range(num_classes)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j],
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')

def plot_confusion_matrix_fix(cm, num_classes, normalize=False, labels = None, stat_type='patch', cmap=plt.cm.Blues, save=None):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """

    classes = range(num_classes)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)

    if stat_type == 'patch':
        plt.title('Patch-Level Confusion Matrix')
    else:
        plt.title('ROI-Level Confusion Matrix')
    #plt.colorbar()
    tick_marks = np.arange(len(classes))

    if labels:
        plt.xticks(tick_marks, labels, rotation=45)
        plt.yticks(tick_marks, labels)
    else:
        plt.xticks(tick_marks, classes, rotation=45)
        plt.yticks(tick_marks, classes)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j],
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout(pad=1.5)

    if stat_type == 'patch':
        plt.ylabel('True Patch labels')
        plt.xlabel('Predicted Patch labels')
    else:
        plt.ylabel('True ROI Labels')
        plt.xlabel('Predicted ROI Labels')
    if save:
        plt.savefig('{}.png'.format(save))
