# ======================
# filename: stats.py
# ======================
#
# Provides a function for showing how labels are distributed given a DataFrame
# containing a list of all patches 

import numpy as np
import matplotlib.pyplot as plt

LABEL_COLORS = ['green', 'purple', 'orange', 'red']
LABEL_NAMES = ['Benign', 'Papilloma', 'Dysplasia', 'Carcinoma']

def plot_label_distribution(df, labels, label_key='label', label_names=LABEL_NAMES, label_colors=LABEL_COLORS, save=None):
    n = len(labels)
    label_counts = np.zeros(n)
    total_counts = len(df)
    idxs = np.arange(n)
    labels_h = []
    labels_c = []

    for idx in range(n):
        label = labels[idx]
        label_counts[idx] = len(df[df[label_key] == label])
        labels_h.append(label_names[idx])
        labels_c.append(label_colors[idx])

    perc = label_counts / total_counts

    for idx in range(n):
        p = round(perc[idx] * 100, 2)
        lab = '{} {}%'.format(labels_h[idx], p)
        labels_h[idx] = lab

    # plot it all
    plt.title('Class Distrubution of {} total patches'.format(total_counts))
    b1 = plt.bar(idxs, height=perc, align='center', color=labels_c)
    plt.legend(b1, labels_h)

    if save:
        plt.savefig(save)
    plt.show()

def get_label_coverage_for_roi(labels, label_classes=[0,1,2,3]):
    """
    Take in a 1-D vector of labels and calculates how often each label occurs within the whole set of given labels.
    The sum of the returned vector is 1.

    The labels vector is assumed to contain labels 0,1,2,...,N, where N is the total number of classes minus one (because first class is zero not one).
    This can be used to let a medical professional decide whether a tissue sample should be taken or not.

    For instance with the labels of 0 = 'benign', 1 = 'papilloma', and 2 = 'carcinoma' you might have a labels list = [1,2,2,2,0,0,0,1,0,0]
    Then this function would calculate the following stats.

    >> get_probability_matrix_for_roi([1,2,2,2,0,0,0,1,0,0])
    array([0.5, 0.2, 0.3])

    """
    total_items = len(labels)

    # we assume the labels start with zero and are digits

    # the index will represent each label
    label_counts = np.zeros(len(label_classes))

    for label in label_classes:
        label_counts[label] = len(np.flatnonzero(labels == label))

    return label_counts / total_items


def should_take_tissue_sample(label_coverage_stats, threshold_label=2, threshold_value=0.5):
    """
    Returns True i.e. recommends taking a tissue sample if the number of labels in a region at or above the threshhold label value exceeds
    the threshhold_value (DEFAULT = 0.5)
    """
    prob = 0.0

    # start index
    j = threshold_label
    while j < len(label_coverage_stats):
        prob += label_coverage_stats[j]
        j += 1

    if prob > threshold_value:
        return True
    else:
        return False
