import pandas as pd
import os
import numpy as np
import cv2

def create_folder_paths(root_dir, folder_id):
    base_dir = os.path.join(root_dir, folder_id)
    test_dir = os.path.join(base_dir, 'test')
    train_dir = os.path.join(base_dir, 'train')
    
    return base_dir, train_dir, test_dir
    
def read_patch_files(base_dir):
    test_patches = pd.read_csv(os.path.join(base_dir, 'test_patches.csv'))
    train_patches = pd.read_csv(os.path.join(base_dir, 'train_patches.csv'))
    
    return train_patches, test_patches
    
def load_data(data, src_dir, patch_size=64):
    
    size = len(data)
    images = np.zeros((size, patch_size, patch_size, 3), dtype=np.float32)
    labels = np.zeros(size, dtype=np.int64)
    
    for idx, row in data.iterrows():
        img = cv2.imread(os.path.join(src_dir, row['patch_name']))
        lab = row['label_id']
        images[idx] = img
        labels[idx] = lab
        
    return images, labels
