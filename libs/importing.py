# ======================
# filename: importing.py
# ======================
#
# This file contains classes which are wrappers for importing files
#
# For importing new data in a new format all you need to do is subclass Importer
# and override the `import_image_data` method
#
# There are many examples of doing so in this file. Importer provides utility functions
# for generating unique ids for importing files as well as loading matlab files

import os
import uuid
import shutil
import pandas as pd
import cv2
import numpy as np
import scipy.io as sio
import re
from glob import glob

def simple_quality_check(quality):
    """
    Returns True if quality is good or maybe, otherwise returns False.
    """
    if quality == 'good' or quality == 'maybe':
        return True
    else:
        return False

class Importer(object):

    def __init__(self, root_dir, out_dir, prefix):
        self.set_root(root_dir)
        self.set_outdir(out_dir)
        self.prefix = prefix
        self.images_df = None
        self.roi_df = None
        self.logger = None

    def set_logger(self, logger):
        self.logger = logger

    def prepare_dir(d):
        if os.path.exists(d):
            shutil.rmtree(d)
        os.makedirs(d)
        os.makedirs(os.path.join(d, 'images'))
        os.makedirs(os.path.join(d, 'contours'))

    def set_root(self, root_dir):
        if not os.path.isdir(root_dir):
            raise Exception('root {} does not exist or is not a directory.'.format(root_dir))
        self.root_dir = root_dir

    def set_outdir(self, out_dir):
        if not os.path.isdir(out_dir):
            raise Exception('output directory {} does not exist or is not a directory.'.format(out_dir))
        self.out_dir = out_dir

    def get_outdir(self):
        return self.out_dir

    def get_root(self):
        return self.root_dir

    def gen_uuid(self):
        return str(uuid.uuid4())[:8]

    def safe_gen_uuid(self, values):
        iters = 0
        max_iters = 5
        while True:
            uuid = self.gen_uuid()
            if uuid in values:
                iters += 1
                continue
            else:
                break
            if iters >= max_iters:
                raise Exception('unable to generate unique uuid: {}'.format(uuid))
        return uuid

    def make_id(self, uuid):
        return '{}-{}'.format(self.prefix, uuid)

    def to_int64(self, vector):
        return np.int64(vector)

    def load_matlab_mat_file(self, path_to_file):
        return sio.loadmat(path_to_file, matlab_compatible=True)

    def delete_next_to_last_entry(self, vector):
        entry_index = len(vector) - 2
        return np.delete(vector, entry_index, axis=0)

    def import_image_data(self):
        pass


class OriginalDataImporter(Importer):

    def __init__(self, root_dir, out_dir, info_df, prefix = 'odi'):
        Importer.__init__(self, root_dir, out_dir, prefix)
        self.info_df = info_df


    def import_image_data(self, quality_check_fn=simple_quality_check):

        # in/out directories
        root = self.get_root()
        out = self.get_outdir()
        img_out = os.path.join(out, 'images')
        roi_out = os.path.join(out, 'contours')

        # counters
        images_count = 0
        normal_roi_count = 0
        extended_roi_count = 0
        failed_quality_check = 0

        # the paths to the images and contours/ROI data
        images = glob(os.path.join(root, '**', '**', '*NBI*mat'))

        # patient number regex
        pat_number_regex = re.compile('.*Pat\.\s([0-9]+)', re.IGNORECASE)
        pat_lesion_number = re.compile('.*L.*sion\s(.*)/', re.IGNORECASE)

        img_df = pd.DataFrame(columns=['id', 'name', 'original_name', 'quality'])
        roi_df = pd.DataFrame(columns=['image_id', 'id', 'name', 'label'])

        roi_idx = 0
        img_idx = 0

        for path_to_mat in images:
            if self.logger:
                self.logger.info('processing: {}'.format(path_to_mat))

            mat = self.load_matlab_mat_file(path_to_mat)
            image = mat['data']['bild'][0][0]

            original_name = os.path.basename(path_to_mat)

            uuid = self.gen_uuid()
            img_id = self.make_id(uuid)
            img_name = '{}.jpg'.format(img_id)
            new_img_path = os.path.join(img_out, img_name)

            # find the label associated with this image
            pat_number = pat_number_regex.match(path_to_mat).groups()[0]
            pat_number = int(pat_number)
            lesion_number = pat_lesion_number.match(path_to_mat).groups()[0]
            lesion_number = int(lesion_number)

            pat = self.info_df[self.info_df['pat_nr'] == pat_number]
            pat_lesion = pat[pat['lesion_nr'] == lesion_number]

            label = pat_lesion['patho'].values[0]
            rating = pat_lesion['rating'].values[0]

            if not quality_check_fn(rating):
                if self.logger:
                    self.logger.info('image: {}, skipping import because rating is: {}.'.format(original_name, rating))
                failed_quality_check += 1
                continue

            if self.logger:
                self.logger.info('image: {}, pat/lesion# {}:{}, label {}'.format(img_name, pat_number, lesion_number, label))

            # save image
            cv2.imwrite(new_img_path, cv2.cvtColor(image, cv2.COLOR_BGR2RGB))
            if self.logger:
                self.logger.info('writing flipped channel image to file at: {}'.format(new_img_path))

            images_count += 1

            # add metadata to the images_df
            img_df.loc[img_idx] = [img_id, img_name, original_name,rating]
            img_idx += 1

            if len(mat['data']['konturen'][0][0]) > 0:

                # grab the contours
                raw_contours = mat['data']['konturen'][0][0][0]

                for k in raw_contours:
                    k_id = self.safe_gen_uuid(roi_df['id'].values)
                    outk = self.delete_next_to_last_entry(k)
                    outk = self.to_int64(outk)
                    k_df = pd.DataFrame(outk)
                    k_name = '{}.csv'.format(k_id)
                    k_df.to_csv(os.path.join(roi_out, k_name), header=None, index=False)

                    # append roi data to the DataFrame
                    roi_df.loc[roi_idx] = [img_id, k_id, k_name, label]
                    roi_idx += 1

                    normal_roi_count += 1

            # check for extended data
            # extended data can only be benign
            if pat_lesion['extended'].values[0]:
                extended_path = os.path.join(self.get_root(), 'extended', '*.csv')
                extended_benign_rois = glob(extended_path)

                for ebr_path in extended_benign_rois:
                    tmp_name = original_name.replace('.mat', '')
                    if tmp_name in ebr_path:
                        label_id = 1 # one is benign
                        k_df = pd.read_csv(ebr_path, header=None, index_col=False)
                        k_id = self.safe_gen_uuid(roi_df['id'].values)
                        k_name = '{}.csv'.format(k_id)
                        k_df.to_csv(os.path.join(roi_out, k_name), header=None, index=False)

                        roi_df.loc[roi_idx] = [img_id, k_id, k_name, label_id]
                        roi_idx += 1

                        if self.logger:
                            self.logger.info('[extended ROI]: {} as {}'.format(original_name, k_name))
                        extended_roi_count += 1

        # save the data frames
        img_df.to_csv(os.path.join(out, 'images.csv'), index = False)
        roi_df.to_csv(os.path.join(out, 'contours.csv'), index = False)

        # Display Summary Data
        if self.logger:
            print('===============')
            print('====summary====')
            print('===============')
            print('images imported: {}'.format(images_count))
            print('images failed quality check: {}'.format(failed_quality_check))
            print('ROIs total: {}'.format(normal_roi_count + extended_roi_count))
            print('ROIs extended: {}'.format(extended_roi_count))
            print('ROIs matlab: {}'.format(normal_roi_count))

        # set the data frames
        self.images_df = img_df
        self.roi_df = roi_df

        return img_df, roi_df


# Regex's for parsing matlab files
NORMAL_1_RE = re.compile('.*normal', re.IGNORECASE)
NORMAL_2_RE = re.compile('.*gesund', re.IGNORECASE)
PAPILLOM_RE = re.compile('.*Papillom', re.IGNORECASE)
MATLAB_CARCINOMA_RE = re.compile('(.*)Karzinom', re.IGNORECASE)
CARCINOMA_1_RE = re.compile('SCC')

class MatlabDataImporter(Importer):

    def __init__(self, root_dir, out_dir, ratings_df, prefix = 'mdi'):
        Importer.__init__(self, root_dir, out_dir, prefix)
        self.ratings_df = ratings_df

    def get_label_id_for_string(self, s):
        if PAPILLOM_RE.match(s):
            return 2 # papilloma
        elif MATLAB_CARCINOMA_RE.match(s) or CARCINOMA_1_RE.match(s):
            return 4 # carcinoma
        elif NORMAL_1_RE.match(s) or NORMAL_2_RE.match(s) or 'Normale Stimmlippe' in s:
            return 1 # benign
        elif ('Dysplasie' in s) or ('dysplasia' in s):
            return 3 # dysplasia
        else:
            print(s)
            raise Exception('Unable to determine label_id for: {}'.format(s))

    def import_image_data(self, quality_check_fn=simple_quality_check):
        # in/out directories
        root = self.get_root()
        out = self.get_outdir()
        img_out = os.path.join(out, 'images')
        roi_out = os.path.join(out, 'contours')

        # counters for summary statistics
        extended_roi_count = 0
        normal_roi_count = 0
        images_count = 0
        failed_quality_check = 0

        # the paths to the images and contours/ROI data
        images = glob(os.path.join(root, '*mat'))


        img_df = pd.DataFrame(columns=['id', 'name', 'original_name','quality'])
        roi_df = pd.DataFrame(columns=['image_id', 'id', 'name', 'label'])

        roi_idx = 0
        img_idx = 0

        for path_to_mat in images:
            mat = self.load_matlab_mat_file(path_to_mat)
            image = mat['data']['bild'][0][0]

            original_name = os.path.basename(path_to_mat)

            entry = self.ratings_df[self.ratings_df['name'] == original_name]
            rating = entry['rating'].values[0]
            if not quality_check_fn(rating):
                failed_quality_check += 1
                if self.logger:
                    self.logger.info('image: {} skipping import due to quality check value: {}'.format(original_name, rating))
                continue

            # get all the labels
            labels = mat['data']['info_2_contours'][0][0][0]

            # generate UUID
            uuid = self.gen_uuid()
            img_id = self.make_id(uuid)
            img_name = '{}.jpg'.format(img_id)
            new_img_path = os.path.join(img_out, img_name)

            if self.logger:
                self.logger.info('image: {} has N={} labels.'.format(img_name, len(labels)))

            # save image
            cv2.imwrite(new_img_path, cv2.cvtColor(image, cv2.COLOR_BGR2RGB))
            if self.logger:
                self.logger.info('writing flipped channel image to file at: {}'.format(new_img_path))
            images_count += 1

            # add metadata to the images_df
            img_df.loc[img_idx] = [img_id, img_name, original_name, rating]
            img_idx += 1

            # grab the contours
            raw_contours = mat['data']['konturen'][0][0][0]

            j = 0
            for k in raw_contours:
                k_id = self.safe_gen_uuid(roi_df['id'].values)
                outk = self.delete_next_to_last_entry(k)
                outk = self.to_int64(outk)
                k_df = pd.DataFrame(outk)
                k_name = '{}.csv'.format(k_id)
                k_df.to_csv(os.path.join(roi_out, k_name), header=None, index=False)

                # grab the associated label
                label_vec = labels[j][0].tolist()
                label_str = ''.join(label_vec)
                label_id = self.get_label_id_for_string(label_str)

                if self.logger:
                    self.logger.info('extracted label [{}:{}] for contour: {} in image {}.'.format(label_str, label_id, k_id, img_id))

                # append roi data to the DataFrame
                roi_df.loc[roi_idx] = [img_id, k_id, k_name, label_id]
                roi_idx += 1
                j += 1
                normal_roi_count += 1

            # check for extended data
            # extended data can only be benign
            if entry['extended'].values[0]:
                extended_path = os.path.join(self.get_root(), 'extended', '*.csv')
                extended_benign_rois = glob(extended_path)

                for ebr_path in extended_benign_rois:
                    tmp_name = original_name.replace('.mat', '')
                    if tmp_name in ebr_path:
                        label_id = 1 # one is benign
                        k_df = pd.read_csv(ebr_path, header=None, index_col=False)
                        k_id = self.safe_gen_uuid(roi_df['id'].values)
                        k_name = '{}.csv'.format(k_id)
                        k_df.to_csv(os.path.join(roi_out, k_name), header=None, index=False)

                        roi_df.loc[roi_idx] = [img_id, k_id, k_name, label_id]
                        roi_idx += 1

                        if self.logger:
                            self.logger.info('[extended ROI]: {} as {}'.format(original_name, k_name))
                        extended_roi_count += 1

        # save the data frames
        img_df.to_csv(os.path.join(out, 'images.csv'), index = False)
        roi_df.to_csv(os.path.join(out, 'contours.csv'), index = False)

        # set the data frames
        self.images_df = img_df
        self.roi_df = roi_df

        # Display Summary Data
        if self.logger:
            print('===============')
            print('====summary====')
            print('===============')
            print('images imported: {}'.format(images_count))
            print('images failed quality check: {}'.format(failed_quality_check))
            print('ROIs total: {}'.format(normal_roi_count + extended_roi_count))
            print('ROIs extended: {}'.format(extended_roi_count))
            print('ROIs matlab: {}'.format(normal_roi_count))

        return img_df, roi_df


class ImportPooler(object):

    def __init__(self, source_dirs=[], logger = None):
        self.source_dirs = source_dirs
        self.logger = logger

    def run_merge(self):
        run_uuid = str(uuid.uuid4())[:8]
        out_images_dir = os.path.join(run_uuid, 'images')
        out_contours_dir = os.path.join(run_uuid, 'contours')

        # create the output directory
        os.makedirs(run_uuid)
        os.makedirs(out_images_dir)
        os.makedirs(out_contours_dir)

        # counters
        image_counter = 0
        roi_counter = 0

        images_frames = []
        roi_frames = []

        # iterate across all our source directories
        for d in self.source_dirs:
            if not os.path.isdir(d):
                raise Exception('{} is not a directory.'.format(d))

            # resources
            images_dir = os.path.join(d, 'images')
            roi_dir = os.path.join(d, 'contours')
            images_csv = os.path.join(d, 'images.csv')
            roi_csv = os.path.join(d, 'contours.csv')

            all_images = glob('{}/*.jpg'.format(images_dir))
            all_contours = glob('{}/*.csv'.format(roi_dir))

            # load the csv files as data frames
            images_frames.append(pd.read_csv(images_csv))
            roi_frames.append(pd.read_csv(roi_csv))

            for img_path in all_images:

                # check if we have an image id conflict - throw error if we do
                img_basename = os.path.basename(img_path)
                dest_img_path = os.path.join(out_images_dir, img_basename)
                if os.path.exists(dest_img_path):
                    raise Exception('image: {} already exists'.format(img_basename))

                shutil.copy2(img_path, out_images_dir)
                if self.logger:
                    self.logger.info('copy: {} to {}'.format(img_path, out_images_dir))

                image_counter += 1

            for roi_path in all_contours:
                roi_basename = os.path.basename(roi_path)
                dest_roi_path = os.path.join(out_contours_dir, roi_basename)
                if os.path.exists(dest_roi_path):
                    raise Exception('roi: {} already exists.'.format(roi_basename))

                shutil.copy2(roi_path, out_contours_dir)
                if self.logger:
                    self.logger.info('copy: {} to {}'.format(roi_path, out_contours_dir))

                roi_counter += 1

        images_df = pd.concat(images_frames)
        roi_df = pd.concat(roi_frames)

        # show summary
        if self.logger:
            print('===============')
            print('====summary====')
            print('===============')
            print('images merged: {}'.format(image_counter))
            print('ROIs merged: {}'.format(roi_counter))
            print('images DF size: {}'.format(len(images_df)))
            print('ROIs DF size: {}'.format(len(roi_df)))


        # save to file
        images_df.to_csv( os. path.join(run_uuid, 'images.csv'), index = False )
        roi_df.to_csv( os. path.join(run_uuid, 'contours.csv'), index = False )


class DataSource(object):

    def __init__(self, root):
        self.set_root(root)
        self.images_df = pd.read_csv(os.path.join(root, 'images.csv'))
        self.contours_df = pd.read_csv(os.path.join(root, 'contours.csv'))
        self.images_dir = os.path.join(root, 'images')
        self.contours_dir = os.path.join(root, 'contours')

        # empty data that must be loaded
        self.image_names = []
        self.image_ids = []
        self.image_data = []
        self.roi_ids = []
        self.roi_data = []
        self.roi_labels  = []

    def load_data(self):
        """
        Loads the Image Names, Image Matrix Data, ROI Ids, ROI Matrices, and Labels.
        """
        image_names = []
        image_ids = []
        image_data = []
        roi_ids = []
        roi_data = []
        label_data = []

        for idx, row in self.images_df.iterrows():
            image_id = row['id']
            image_name = row['name']
            img = cv2.imread(os.path.join(self.images_dir, image_name))
            image_data.append(img)
            image_ids.append(image_id)
            image_names.append(image_name)

            # find the image ROIs
            t_roi_data = []
            t_roi_ids = []
            t_label_data = []

            for idx, row in (self.contours_df[self.contours_df['image_id'] == image_id]).iterrows():
                k_id = row['id']
                k_name = row['name']
                label_id = row['label']
                roi = pd.read_csv(os.path.join(self.contours_dir, k_name), header=None, index_col=False).as_matrix()

                t_roi_data.append(roi)
                t_roi_ids.append(k_id)
                t_label_data.append(label_id)
            roi_ids.append(t_roi_ids)
            roi_data.append(t_roi_data)
            label_data.append(t_label_data)

        # store references to the loaded data
        self.image_names = image_names
        self.image_ids = image_ids
        self.image_data = image_data
        self.roi_ids = roi_ids
        self.roi_data = roi_data
        self.roi_labels = label_data

    def set_root(self, root_dir):
        if not os.path.isdir(root_dir):
            raise Exception('{} is not a directory'.format(root_dir))
        self.root_dir = root_dir

    def get_index_for_image_id(self, image_id):
        for idx in range(len(self.image_ids)):
            if self.image_ids[idx] == image_id:
                    return idx
        return -1

    def get_data_at(self, index):
        return (self.image_ids[index], self.image_names[index],
                self.image_data[index], self.roi_ids[index],
                self.roi_data[index], self.roi_labels[index])


    def get_root(self):
        return self.root_dir


class PatchDataReader(object):

    def __init__(self, root_dir, df, patch_size = 64):
        self.set_root(root_dir)
        self.df = df
        self.patch_size = patch_size
        self.images = []
        self.labels = []

    def get_images(self):
        return self.images

    def get_labels(self):
        return self.labels

    def read_data(self):

        size = len(self.df)

        images = np.zeros((size, self.patch_size, self.patch_size, 3), dtype=np.float32)
        labels = np.zeros(size, dtype=np.int64)

        for idx, row in self.df.iterrows():

            patch_path = os.path.join(self.root_dir, row['patch_name'])
            if not os.path.exists(patch_path):
                raise Exception('path to patch: {} does not exist.'.format(patch_path))

            img = cv2.imread(os.path.join(self.root_dir, row['patch_name']))
            lab = row['label_id']
            images[idx] = img
            labels[idx] = lab

        self.images = images
        self.labels = labels

        return images, labels

    def set_root(self, root_dir):
        if not os.path.isdir(root_dir):
            raise Exception('{} is not a directory'.format(root_dir))
        self.root_dir = root_dir

    def get_root(self):
        return self.root_dir
