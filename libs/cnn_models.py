# ======================
# filename: cnn_models.py
# ======================
#
# This file contains tensorflow / keras model implementations of convolutional neural networks.
#
# It provides some useful functions for generating optimizers (for instance RMSprop)
# and an early stopping function and an annealing function which reduces the learning rate when learning has stalled
#
# A lot of the models here were tried and tweaked and left in the code
# Most of the research relevant models come at the end of this file, these have the prefixes:  
# 1. model_from_previous_research
# 2. model_3layer

import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Conv2D, MaxPool2D, Activation
from keras.layers.normalization import BatchNormalization
from keras.optimizers import RMSprop
from keras.callbacks import ReduceLROnPlateau, EarlyStopping

def optimizer(learning_rate=0.001):
    return RMSprop(lr=learning_rate, rho=0.9, epsilon=1e-8, decay=0.0)

def annealer(patience=3):
    return ReduceLROnPlateau(monitor='val_acc', patience=patience, verbose=1, factor=0.5, min_lr=0.00001)



def early_stopping():
    return EarlyStopping(monitor='val_loss', min_delta=0.0001, patience=2, verbose=1, mode='auto')

def model_simple_v1_2class():
    """
    This model contains zero regularization methods such as BatchNormalization and Dropout.
    It is used to find out the right parameters for the loss and optimization functions.
    """

    model = Sequential()

    model.add(Conv2D(filters = 32, kernel_size = (3,3),padding = 'Same', input_shape = (64,64,3)))
    model.add(Activation('relu'))
    model.add(Conv2D(filters = 32, kernel_size = (3,3), padding = 'Same'))
    model.add(Activation('relu'))
    model.add(MaxPool2D(pool_size=(2,2)))

    model.add(Conv2D(filters = 64, kernel_size = (3,3), padding = 'Same'))
    model.add(Activation('relu'))
    model.add(Conv2D(filters = 64, kernel_size = (3,3),padding = 'Same'))
    model.add(Activation('relu'))
    model.add(MaxPool2D(pool_size=(2,2), strides=(2,2)))


    model.add(Flatten())
    model.add(Dense(512))
    model.add(Activation('relu'))
    model.add(Dense(2, kernel_initializer='normal', activation = "sigmoid"))

    return model

def model_simple_v1_3class():
    """
    This model contains zero regularization methods such as BatchNormalization and Dropout.
    It is used to find out the right parameters for the loss and optimization functions.
    """

    model = Sequential()

    model.add(Conv2D(filters = 32, kernel_size = (3,3),padding = 'Same', input_shape = (64,64,3)))
    model.add(Activation('relu'))
    model.add(Conv2D(filters = 32, kernel_size = (3,3), padding = 'Same'))
    model.add(Activation('relu'))
    model.add(MaxPool2D(pool_size=(2,2)))

    model.add(Conv2D(filters = 64, kernel_size = (3,3), padding = 'Same'))
    model.add(Activation('relu'))
    model.add(Conv2D(filters = 64, kernel_size = (3,3),padding = 'Same'))
    model.add(Activation('relu'))
    model.add(MaxPool2D(pool_size=(2,2), strides=(2,2)))


    model.add(Flatten())
    model.add(Dense(512))
    model.add(Activation('relu'))
    model.add(Dense(3))
    model.add(Activation('softmax'))

    return model

def model_simple_v2_2class():
    """
    This is a simple model with 2 hidden layers and a sigmoid activation function on the output layer.

    It add regularization via dropout.
    """

    model = Sequential()

    model.add(Conv2D(filters = 32, kernel_size = (3,3),padding = 'Same', input_shape = (64,64,3)))
    model.add(Activation('relu'))
    model.add(Conv2D(filters = 32, kernel_size = (3,3), padding = 'Same'))
    model.add(Activation('relu'))
    model.add(MaxPool2D(pool_size=(2,2)))
    model.add(Dropout(0.25))

    model.add(Conv2D(filters = 64, kernel_size = (3,3), padding = 'Same'))
    model.add(Activation('relu'))
    model.add(Conv2D(filters = 64, kernel_size = (3,3),padding = 'Same'))
    model.add(Activation('relu'))
    model.add(MaxPool2D(pool_size=(2,2), strides=(2,2)))
    model.add(Dropout(0.25))


    model.add(Flatten())
    model.add(Dense(512))
    model.add(Activation('relu'))
    model.add(Dropout(0.5))
    model.add(Dense(2, kernel_initializer='normal', activation = "sigmoid"))

    return model

def model_simple_v2_3class_dropout():
    """

    It contains zero regularization methods such as BatchNormalization and Dropout.
    It is used to find out the right parameters for the loss and optimization functions.
    """

    model = Sequential()

    model.add(Conv2D(filters = 32, kernel_size = (3,3),padding = 'Same', input_shape = (64,64,3)))
    model.add(Activation('relu'))
    model.add(Conv2D(filters = 32, kernel_size = (3,3), padding = 'Same'))
    model.add(Activation('relu'))
    model.add(MaxPool2D(pool_size=(2,2)))
    model.add(Dropout(0.25))

    model.add(Conv2D(filters = 64, kernel_size = (3,3), padding = 'Same'))
    model.add(Activation('relu'))
    model.add(Conv2D(filters = 64, kernel_size = (3,3),padding = 'Same'))
    model.add(Activation('relu'))
    model.add(MaxPool2D(pool_size=(2,2), strides=(2,2)))
    model.add(Dropout(0.25))


    model.add(Flatten())
    model.add(Dense(1024))
    model.add(Activation('relu'))
    model.add(Dropout(0.5))
    model.add(Dense(3))
    model.add(Activation('softmax'))

    return model


def model_3layer_v2_2class():
    """
    This is a model with 3 hidden layers and a sigmoid activation function on the output layer.

    It adds regularization via dropout.
    """

    model = Sequential()

    model.add(Conv2D(filters = 64, kernel_size = (3,3),padding = 'Same', input_shape = (64,64,3)))
    model.add(Activation('relu'))
    model.add(Conv2D(filters = 48, kernel_size = (3,3), padding = 'Same'))
    model.add(Activation('relu'))
    model.add(MaxPool2D(pool_size=(2,2)))
    model.add(Dropout(0.25))

    model.add(Conv2D(filters = 128, kernel_size = (3,3), padding = 'Same'))
    model.add(Activation('relu'))
    model.add(Conv2D(filters = 196, kernel_size = (5,5),padding = 'Same'))
    model.add(Activation('relu'))
    model.add(MaxPool2D(pool_size=(2,2), strides=(2,2)))
    model.add(Dropout(0.25))

    model.add(Conv2D(filters = 64, kernel_size = (3,3), padding = 'Same'))
    model.add(Activation('relu'))
    model.add(Conv2D(filters = 64, kernel_size = (3,3),padding = 'Same'))
    model.add(Activation('relu'))
    model.add(MaxPool2D(pool_size=(2,2), strides=(2,2)))
    model.add(Dropout(0.25))


    model.add(Flatten())
    model.add(Dense(1024))
    model.add(Activation('relu'))
    model.add(Dropout(0.5))
    model.add(Dense(2, kernel_initializer='normal', activation = "sigmoid"))

    return model

def model_3layer(num_classes=3):
    """
    This is a model with 3 hidden layers and a sigmoid activation function on the output layer.

    It adds regularization via dropout.
    """

    model = Sequential()

    model.add(Conv2D(filters = 64, kernel_size = (3,3),padding = 'Same', input_shape = (64,64,3)))
    model.add(Activation('relu'))
    model.add(Conv2D(filters = 48, kernel_size = (3,3), padding = 'Same'))
    model.add(Activation('relu'))
    model.add(MaxPool2D(pool_size=(2,2)))
    model.add(Dropout(0.25))

    model.add(Conv2D(filters = 128, kernel_size = (3,3), padding = 'Same'))
    model.add(Activation('relu'))
    model.add(Conv2D(filters = 196, kernel_size = (5,5),padding = 'Same'))
    model.add(Activation('relu'))
    model.add(MaxPool2D(pool_size=(2,2), strides=(2,2)))
    model.add(Dropout(0.25))

    model.add(Conv2D(filters = 64, kernel_size = (3,3), padding = 'Same'))
    model.add(Activation('relu'))
    model.add(Conv2D(filters = 64, kernel_size = (3,3),padding = 'Same'))
    model.add(Activation('relu'))
    model.add(MaxPool2D(pool_size=(2,2), strides=(2,2)))
    model.add(Dropout(0.25))


    model.add(Flatten())
    model.add(Dense(4096))
    model.add(Activation('relu'))
    model.add(Dropout(0.25))
    model.add(Dense(num_classes))
    model.add(Activation('softmax'))

    return model



def model_from_previous_research_2class(input_shape=(64,64,3)):
    model = Sequential()

    model.add(Conv2D(filters=64, kernel_size=(5,5), strides=2, padding='same', input_shape=input_shape))
    model.add(Activation('relu'))
    model.add(MaxPool2D(pool_size=(3,3), strides=2))

    model.add(Conv2D(filters=192, kernel_size=(4,4), strides=1, padding='same'))
    model.add(Activation('relu'))
    model.add(MaxPool2D(pool_size=(3,3), strides=2))

    model.add(Conv2D(filters=256, kernel_size=(3,3), strides=1, padding='same'))
    model.add(Activation('relu'))

    model.add(Conv2D(filters=256, kernel_size=(3,3), strides=1, padding='same'))
    model.add(Activation('relu'))

    model.add(Conv2D(filters=256, kernel_size=(3,3), strides=1, padding='same'))
    model.add(Activation('relu'))
    model.add(MaxPool2D(pool_size=(2,2), strides=1))

    model.add(Flatten())
    model.add(Dense(4096))
    model.add(Activation('relu'))
    model.add(Dense(4096))
    model.add(Activation('relu'))
    model.add(Dense(2, kernel_initializer='normal', activation='sigmoid'))

    return model

def model_from_previous_research_2class_dropout(input_shape=(64,64,3)):
    model = Sequential()

    model.add(Conv2D(filters=64, kernel_size=(5,5), strides=2, padding='same', input_shape=input_shape))
    model.add(Activation('relu'))
    model.add(MaxPool2D(pool_size=(3,3), strides=2))

    model.add(Conv2D(filters=192, kernel_size=(4,4), strides=1, padding='same'))
    model.add(Activation('relu'))
    model.add(MaxPool2D(pool_size=(3,3), strides=2))
    model.add(Dropout(0.25))

    model.add(Conv2D(filters=256, kernel_size=(3,3), strides=1, padding='same'))
    model.add(Activation('relu'))

    model.add(Conv2D(filters=256, kernel_size=(3,3), strides=1, padding='same'))
    model.add(Activation('relu'))

    model.add(Conv2D(filters=256, kernel_size=(3,3), strides=1, padding='same'))
    model.add(Activation('relu'))
    model.add(MaxPool2D(pool_size=(2,2), strides=1))
    model.add(Dropout(0.25))

    model.add(Flatten())
    model.add(Dense(4096))
    model.add(Activation('relu'))
    model.add(Dense(4096))
    model.add(Activation('relu'))
    model.add(Dropout(0.5))
    model.add(Dense(2, kernel_initializer='normal', activation='sigmoid'))

    return model


def model_from_previous_research(input_shape=(64,64,3), num_classes=2):
    model = Sequential()

    model.add(Conv2D(filters=64, kernel_size=(5,5), strides=2, padding='same', input_shape=input_shape))
    model.add(Activation('relu'))
    model.add(MaxPool2D(pool_size=(3,3), strides=2))

    model.add(Conv2D(filters=192, kernel_size=(4,4), strides=1, padding='same'))
    model.add(Activation('relu'))
    model.add(MaxPool2D(pool_size=(3,3), strides=2))

    model.add(Conv2D(filters=256, kernel_size=(3,3), strides=1, padding='same'))
    model.add(Activation('relu'))

    model.add(Conv2D(filters=256, kernel_size=(3,3), strides=1, padding='same'))
    model.add(Activation('relu'))

    model.add(Conv2D(filters=256, kernel_size=(3,3), strides=1, padding='same'))
    model.add(Activation('relu'))
    model.add(MaxPool2D(pool_size=(2,2), strides=1))


    model.add(Flatten())
    model.add(Dense(4096))
    model.add(Activation('relu'))
    model.add(Dense(4096))
    model.add(Activation('relu'))
    model.add(Dense(num_classes))
    model.add(Activation('softmax'))

    return model

def model_from_previous_research_dropout(input_shape=(64,64,3), num_classes=2):
    model = Sequential()

    model.add(Conv2D(filters=64, kernel_size=(5,5), strides=2, padding='same', input_shape=input_shape))
    model.add(Activation('relu'))
    model.add(MaxPool2D(pool_size=(3,3), strides=2))

    model.add(Conv2D(filters=192, kernel_size=(4,4), strides=1, padding='same'))
    model.add(Activation('relu'))
    model.add(MaxPool2D(pool_size=(3,3), strides=2))
    model.add(Dropout(0.25))

    model.add(Conv2D(filters=256, kernel_size=(3,3), strides=1, padding='same'))
    model.add(Activation('relu'))

    model.add(Conv2D(filters=256, kernel_size=(3,3), strides=1, padding='same'))
    model.add(Activation('relu'))

    model.add(Conv2D(filters=256, kernel_size=(3,3), strides=1, padding='same'))
    model.add(Activation('relu'))
    model.add(MaxPool2D(pool_size=(2,2), strides=1))
    model.add(Dropout(0.25))

    model.add(Flatten())
    model.add(Dense(4096))
    model.add(Activation('relu'))
    model.add(Dense(4096))
    model.add(Activation('relu'))
    model.add(Dropout(0.5))
    model.add(Dense(num_classes))
    model.add(Activation('softmax'))

    return model
