import numpy as np

def scale_normalization(data):
    """
    Divides the RGB values by 255 to normalize the image down to a range between 0-1.
    This makes the learning alogrithms work much faster.

    see: https://en.wikipedia.org/wiki/Feature_scaling

    >>> simple_scale(np.array([100,50,60]))
    array([0.3921, 0.1960, 0.2352])

    """
    return data / 255

def dataset_mean_and_std(data):
    """
    Find the mean image and the standard deviation image for an entire dataset.

    It assumes your data has the dimensionality (N, Width, Height, Channels), where N is the number of image examples you have.

    Returns a mean image and standard deviation image with dimensions (Width, Height, Channels)
    """
    data_mean = np.mean(data, axis=0)
    data_std  = np.std(data, axis=0)

    return (data_mean, data_std)
