import sys
sys.path.append('../')
import numpy as np
import pandas as pd
import libs.utils as utils
import libs.render as render
import os
import shutil
import cv2

# this works with a datasource object

def filter_by_quality(ds, quality):
    if isinstance(quality, (list,)):
        images_df = ds.images_df[ds.images_df['quality'].isin(quality)]
    else:
        images_df = ds.images_df[ds.images_df['quality'] == quality]
    image_ids = images_df['id'].values

    roi_df = ds.contours_df[ds.contours_df['image_id'].isin(image_ids)]

    return images_df, roi_df


def filter_by_quality_and_save(ds, quality, out_dir, logger=None):
    # src dirs
    source_dir = ds.get_root()
    src_img_dir = os.path.join(source_dir, 'images')
    src_roi_dir = os.path.join(source_dir, 'contours')

    # destination dirs
    target_path = os.path.join(source_dir, out_dir)
    out_img_dir = os.path.join(target_path, 'images')
    out_roi_dir = os.path.join(target_path, 'contours')

    # log dirs
    log_dir = os.path.join(target_path, 'filter_log')

    # create directories
    utils.remove_and_create_path(target_path)
    utils.makedirs([out_img_dir, out_roi_dir, log_dir])
    if logger:
        logger.info('[created directories]: {},{},{}'.format(out_img_dir, out_roi_dir, log_dir))

    # apply the filter
    img_df, roi_df = filter_by_quality(ds, quality)

    # save our newly filtered .csv files
    img_df.to_csv(os.path.join(target_path, 'images.csv'), index=False)
    roi_df.to_csv(os.path.join(target_path, 'contours.csv'), index=False)

    for idx, row in ds.images_df.iterrows():
        img_name = row['name']
        img_id = row['id']
        src_img = os.path.join(src_img_dir, img_name)
        res = img_df[img_df['name'] == img_name]

        if len(res) > 0:
            if logger:
                logger.info('[kept]: {}'.format(img_name))
            shutil.copy2(src_img, out_img_dir)
        else:
            # this is not a good image protocol it
            if logger:
                logger.info('[removed]: {}.'.format(img_name))

        if len(res) > 0:
            # copy the contour
            roi_subset = ds.contours_df[ds.contours_df['image_id'] == img_id]
            for j, rs in roi_subset.iterrows():
                rs_name = rs['name']
                shutil.copy2(os.path.join(src_roi_dir, rs_name), out_roi_dir)
                if logger:
                    logger.info('copied: {} to {}'.format(rs_name, out_roi_dir))

        else:
            # Render the whole image and write to log_dir
            img = cv2.imread(src_img)
            roi_subset = ds.contours_df[ds.contours_df['image_id'] == img_id]
            for j, rs in roi_subset.iterrows():
                rs_name = rs['name']
                roi = pd.read_csv(os.path.join(src_roi_dir, rs_name), header=None, index_col=False)
                img = render.draw_roi_on_image(img, roi.as_matrix())
            cv2.imwrite(os.path.join(log_dir, img_name), img)
